<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LeaveRequest extends Model
{
    use SoftDeletes;
    protected $guarded =[];

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\User', 'user_id', 'id')->withTrashed();
    }

    public function getDayTwoDay()
    {
        $start = Carbon::parse($this->start_at);
        $end = Carbon::parse($this->end_at);
        return number_format($start->diffInHours($end)/24,2);
    }
}
