<?php

namespace App\Providers;

use App\Repositories\Statics\StaticCheckRepositoryInterface;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\File;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
            \App\Repositories\UserChecks\UserCheckRepositoryInterface::class,
            \App\Repositories\UserChecks\UserCheckEloquentRepository::class
        );
        $this->app->singleton(
            \App\Repositories\Users\UserRepositoryInterface::class,
            \App\Repositories\Users\UserRepositoryRepository::class
        );
        $this->app->singleton(
            \App\Repositories\LeaveRequests\LeaveRequestRepositoryInterface::class,
            \App\Repositories\LeaveRequests\LeaveRequestRepository::class
        );
        $this->app->singleton(
            \App\Repositories\Positions\PositionRepositoryInterface::class,
            \App\Repositories\Positions\PositionEloquentRepository::class
        );
        $this->app->singleton(
            \App\Repositories\Statics\StaticCheckRepositoryInterface::class,
            \App\Repositories\Statics\StaticEloquentRepository::class
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        DB::listen(function ($query) {
            File::append(
                storage_path('/logs/query.log'),
                '[' . date('Y-m-d H:i:s') . ']' . PHP_EOL . $query->sql . ' [' . implode(', ', $query->bindings) . ']' . PHP_EOL . PHP_EOL
            );
        });
    }
}
