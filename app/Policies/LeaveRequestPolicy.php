<?php

namespace App\Policies;

use App\LeaveRequest;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class LeaveRequestPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param User $user
     * @return mixed
     */
    public function index(User $user): bool
    {
        if ($user) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param User $user
     * @return mixed
     */
    public function create(User $user): bool
    {
        if ($user->role === 'staff') {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param User $user
     * @return mixed
     */
    public function accept(User $user)
    {
        if ($user->role === 'admin') {
            return true;
        }
        return false;
    }
    /**
     * Determine whether the user can update the model.
     *
     * @param User $user
     * @return mixed
     */
    public function deny(User $user)
    {
        if ($user->role === 'admin') {
            return true;
        }
        return false;
    }



    /**
     * Determine whether the user can restore the model.
     *
     * @param User $user
     * @return mixed
     */
    public function store(User $user)
    {
        if ($user->role === 'staff') {
            return true;
        }
        return false;
    }


}
