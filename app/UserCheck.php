<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class UserCheck extends Model
{
    protected $table = 'user_checks';
    protected $guarded = [];

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\User', 'user_id', 'id')->withTrashed();
    }



    public function countHourInDay()
    {
        $in = Carbon::createFromFormat('H:i:s',$this->check_in);
        $out =  Carbon::createFromFormat('H:i:s',$this->check_out);
        return $in->diffInSeconds($out);
    }

}
