<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    protected $table = 'positions';
    protected $fillable = ['name'];

    public function users()
    {
        return $this->belongsToMany(User::class,'positions_users','position_id','user_id')->withTrashed();
    }
}
