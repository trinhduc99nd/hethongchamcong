<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreRequest;
use App\Http\Requests\UpdateInfoRequest;
use App\Http\Requests\UserRequest;
use App\Mail\CreateUserMail;
use App\Repositories\Positions\PositionRepositoryInterface;
use App\Traits\DeleteModelTrait;
use App\Repositories\Users\UserRepositoryInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use RealRashid\SweetAlert\Facades\Alert;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{
    use DeleteModelTrait;

    protected $userRepository;
    protected $positionRepository;

    public function __construct(UserRepositoryInterface $userRepository, PositionRepositoryInterface $positionRepository)
    {
        $this->middleware(['auth']);
        $this->userRepository = $userRepository;
        $this->positionRepository = $positionRepository;
    }

    public function index()
    {
        $users = $this->userRepository->getAll();
        return view('timesheet_management.users.index', compact('users'));
    }

    public function add()
    {
        $positions = $this->positionRepository->getAll();

        return view('timesheet_management.users.add', compact('positions'));
    }

    public function create(UserRequest $request)
    {
        try {
            DB::beginTransaction();
            $request = (object)$request->validated();
            foreach ($request->position as $positionUser) {
                $positionIds[] = $this->positionRepository->firstOrCreatePosition($positionUser)->id;
            }
            $this->userRepository->create($this->userRepository->insertDataUser($request))
                ->positions()->attach($positionIds);
            $details = [
                'email' => $request->email,
                'password' => $request->password
            ];
            Mail::to($request->email)->send(new CreateUserMail($details));
            DB::commit();
            Alert::success('Create User Success', 'Email sent to User')->autoClose(3000);
            return redirect()->route('users.index');
        } catch (\Exception $exception) {
            DB::rollBack();
            Log::error('Message: ' . $exception->getMessage() . 'Line : ' . $exception->getLine());
            return back()->with('error', 'Create used Error');
        }
    }

    public function edit($id)
    {

        $user = $this->userRepository->find($id);
        $positions = $this->positionRepository->getAll();
        foreach ($user->positions as $position) {
            $list[] = $position->id;
        }
        return view('timesheet_management.users.edit', compact('user', 'positions', 'list'));
    }

    public function update($id, StoreRequest $storeRequest)
    {
        try {
            DB::beginTransaction();
            $storeRequest = (object)$storeRequest->validated();
            foreach ($storeRequest->position as $positionUser) {
                $positionIds[] = $this->positionRepository->firstOrCreatePosition($positionUser)->id;
            }
            $this->userRepository->update($id, $this->userRepository->updateDataUser($storeRequest))
                ->positions()->sync($positionIds);
            DB::commit();
            Alert::success('Success', 'Update User Success')->autoClose(3000);
            return redirect()->route('users.index');
        } catch (\Exception $exception) {
            DB::rollBack();
            Log::error('Message: ' . $exception->getMessage() . 'Line : ' . $exception->getLine());
            return back()->with('error', "Update User failed");
        }
    }

    public function delete($id): \Illuminate\Http\JsonResponse
    {

        return $this->deleteModelTrait($id, $this->userRepository);
    }

    public function info()
    {
        $user = Auth::user();
        return view('timesheet_management.users.info', compact('user'));
    }

    public function editInfo($id)
    {
        abort_if(Gate::denies('editInfo',$id), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $user = $this->userRepository->find($id);
        return view('timesheet_management.users.editInfo', compact('user'));
    }

    public function updateInfo($id, UpdateInfoRequest $request)
    {
        abort_if(Gate::denies('editInfo',$id), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $this->userRepository->update($id, $this->userRepository->updateDataUserInfo($request));
        Alert::success('Success', 'Update Info Success')->autoClose(3000);
        return redirect()->route('users.info');
    }

}
