<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\ChangePasswordRequest;
use App\Repositories\Users\UserRepositoryInterface;
use Illuminate\Support\Facades\Hash;

class ChangePasswordController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('timesheet_management.changePassword');
    }

    public function store(ChangePasswordRequest $request, UserRepositoryInterface $userRepository)
    {
        $request = (object)$request->validated();
        $userRepository->find(auth()->id())->update(['password' =>Hash::make($request->new_password)]);
        return redirect('change-password')->with('toast_success','Password change successfully!');
    }
}
