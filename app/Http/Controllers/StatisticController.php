<?php

namespace App\Http\Controllers;

use App\Repositories\Statics\StaticCheckRepositoryInterface;
use App\Repositories\UserChecks\UserCheckRepositoryInterface;
use App\Repositories\Users\UserRepositoryInterface;
use App\Statistic;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Symfony\Component\HttpFoundation\Response;

class StatisticController extends Controller
{
    protected $staticCheckRepository;
    protected $userCheckRepository;

    public function __construct(StaticCheckRepositoryInterface $staticCheckRepository, UserCheckRepositoryInterface $userCheckRepository )
    {
        $this->middleware(['auth']);
        $this->staticCheckRepository = $staticCheckRepository;
        $this->userCheckRepository = $userCheckRepository;
    }

    public function index()
    {
        $years = $this->staticCheckRepository->getYearDistinct();
        return view('timesheet_management.statistic_month.index', compact('years'));
    }

    public function view($id, $year, $month)
    {
        abort_if(Gate::denies('statistic',$id), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $data = $this->staticCheckRepository->getDataUser($id, $year, $month);
        $timesheetInMonths = $this->userCheckRepository->getTimeSheetUserInMonth($month,$year,$id);
        return view('timesheet_management.statistic_month.view', compact('year', 'month','data','timesheetInMonths'));
    }


    public function getMonth($year)
    {
        $month = $this->staticCheckRepository->getMonthDistinct($year);
        return response()->json($month);
    }

    public function getData($year, $month)
    {
        $data = $this->staticCheckRepository->getData($year, $month);
        return response()->json($data);
    }
}
