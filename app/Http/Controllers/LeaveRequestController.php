<?php

namespace App\Http\Controllers;

use App\Http\Requests\DenyRequest;
use App\Http\Requests\LeaveRequest;
use App\Mail\LeaveRequestAdminMail;
use App\Mail\LeaveRequestMail;
use App\Notifications\LeaveRequestNotification;
use App\Repositories\LeaveRequests\LeaveRequestRepositoryInterface;
use App\Repositories\Users\UserRepositoryInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Symfony\Component\HttpFoundation\Response;
use RealRashid\SweetAlert\Facades\Alert;

class LeaveRequestController extends Controller
{
    protected $leaveRequestEloquent;
    protected $userRepository;

    public function __construct(LeaveRequestRepositoryInterface $leaveRequestEloquent, UserRepositoryInterface $userRepository)
    {
        $this->middleware(['auth']);
        $this->leaveRequestEloquent = $leaveRequestEloquent;
        $this->userRepository = $userRepository;
    }

    public function index()
    {
        abort_if(Gate::denies('leave-index'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $leave_requests = $this->leaveRequestEloquent->getAll();
        $leave_requestPersons = $this->leaveRequestEloquent->getListLeaveRequestPerson();
        return view('timesheet_management.leave_requests.index', compact('leave_requests','leave_requestPersons'));
    }

    public function create()
    {
        abort_if(Gate::denies('leave-create'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $user = Auth::user();
        return view('timesheet_management.leave_requests.add', compact('user'));
    }

    public function store(LeaveRequest $request)
    {
        abort_if(Gate::denies('leave-store'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        try {
            $request = (object)$request->validated();
            $this->leaveRequestEloquent->create($this->leaveRequestEloquent->insertDataLeaveRequest($request));
            // send notification
            $users = $this->userRepository->getListAdminUser();
            $data = [
                'title' => 'Yêu cầu xin nghỉ',
                'content' => $request->content,
                'name' => $this->userRepository->getNameUser(),
            ];
            foreach ($users as $user) {
                $user->notify(new LeaveRequestNotification($data));
                Mail::to($user->email)->send(new LeaveRequestAdminMail($data));
            }
            Alert::success('Success', 'Send Request success.Please forward admin approved')->autoClose(10000);
            return redirect()->route('leave_requests.index');
        } catch (\Exception $exception) {
            Log::error('Message: ' . $exception->getMessage() . 'Line : ' . $exception->getLine());
            return back()->with('error', "Send Request failed");
        }
    }

    public function accept($id)
    {
        abort_if(Gate::denies('leave-accept'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $this->leaveRequestEloquent->update($id, ['status' => 'approved']);
        $userId = $this->leaveRequestEloquent->getListLeaveRequestPersonId($id);
        $user = $this->userRepository->getUser($userId);

        $data = [
            'title' => 'Yêu cầu xin nghỉ ',
            'content' => 'Yêu cầu xin nghỉ của bạn đã được chấp nhận',
        ];
        $user->notify(new LeaveRequestNotification($data));
        Mail::to($user->email)->send(new LeaveRequestMail($data));
        Alert::success('Success', 'Request is approved. Notification and email send to user')->autoClose(3000);
        return redirect()->route('leave_requests.index');
    }

    public function deny($id, DenyRequest $request): \Illuminate\Http\RedirectResponse
    {
        abort_if(Gate::denies('leave-deny'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $request = (object)$request->validated();
        $userId = $this->leaveRequestEloquent->getListLeaveRequestPersonId($id);
        $user = $this->userRepository->getUser($userId);
        $this->leaveRequestEloquent->update($id, ['status' => 'unapproved']);
        $data = [
            'title' => 'Yêu cầu xin nghỉ ',
            'content' => 'Yêu cầu xin nghỉ của bạn đã bị từ chối ',
            'reason' => $request->content_deny
        ];
        Mail::to($user->email)->send(new LeaveRequestMail($data));
        $user->notify(new LeaveRequestNotification($data));
        Alert::success('Success', 'Request is unapproved. Notification and email send to user')->autoClose(5000);
        return redirect()->route('leave_requests.index');
    }
}
