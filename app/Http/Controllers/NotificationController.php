<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }
    public function index()
    {
         $userId = Auth::user()->id;
        $notifications = DatabaseNotification::where('notifiable_id',$userId)->orderby('created_at', 'desc')->limit(5)->get(['data','created_at']);
        return response()->json($notifications);
    }

}
