<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCheckRequest;
use App\Http\Requests\StoreProblemRequest;
use App\Mail\LeaveRequestAdminMail;
use App\Notifications\LeaveRequestNotification;
use App\Repositories\UserChecks\UserCheckRepositoryInterface;
use App\Repositories\Users\UserRepositoryInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Mail;
use RealRashid\SweetAlert\Facades\Alert;
use Symfony\Component\HttpFoundation\Response;

class ListCheckController extends Controller
{
    protected $userCheckRepository;

    public function __construct(UserCheckRepositoryInterface $userCheckRepository)
    {
        $this->middleware(['auth']);
        $this->userCheckRepository = $userCheckRepository;
    }

    public function personList()
    {
        $userName = Auth::user()->getFullNameAttribute();
        $listCheckPersons = $this->userCheckRepository->getListWorkingOfDayUser();
        return view('timesheet_management.listCheck.person', compact('listCheckPersons', 'userName'));
    }

    public function peopleList()
    {
        abort_if(Gate::denies('isAdmin'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $listCheckUsers = $this->userCheckRepository->getListUser();
        return view('timesheet_management.listCheck.people', compact('listCheckUsers'));
    }

    public function addProblem()
    {
        abort_if(Gate::denies('isStaff'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        return view('timesheet_management.listCheck.add_problem');
    }

    public function storeProblem(StoreProblemRequest $request, UserRepositoryInterface $userRepository): \Illuminate\Http\RedirectResponse
    {
        abort_if(Gate::denies('isStaff'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $request = (object)$request->validated();
        $users = $userRepository->getListAdminUser();
        $data = [
            'title' => 'Yêu cầu xin chấm công',
            'day' => $request->day,
            'content' => $request->content,
            'name' => $userRepository->getNameUser(),
        ];
        foreach ($users as $user) {
            $user->notify(new LeaveRequestNotification($data));
            Mail::to($user->email)->send(new LeaveRequestAdminMail($data));
        }
        Alert::success('Success', 'Send Request success.Please forward admin changed')->autoClose(10000);
        return redirect()->route('person.personList');
    }

    public function updateCheckOut($id, UserCheckRepositoryInterface $userCheckRepository): \Illuminate\Http\RedirectResponse
    {
        abort_if(Gate::denies('isAdmin'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $userCheckRepository->updateUserCheckOutId($id);
        Alert::success('Success', 'Update Success')->autoClose(3000);
        return redirect()->route('people.peopleList');
    }

    public function viewCheck(UserRepositoryInterface $userRepository)
    {
        abort_if(Gate::denies('isAdmin'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $users = $userRepository->getAll();
        return view('timesheet_management.listCheck.add_check', compact('users'));
    }

    public function storeCheck(StoreCheckRequest $request, UserCheckRepositoryInterface $userCheckRepository)
    {
        abort_if(Gate::denies('isAdmin'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $request = (object)$request->validated();
        $result = $userCheckRepository->updateTimeSheetOfUser($request);
        if (isset($result)) {
            toast('You have working on day of user exists. Thank you', 'info')->width('400px')->position('top-right')->autoClose(5000)->background('#fff');;
            return redirect()->back();
        }
        $this->userCheckRepository->create([
            'working_on_day' => $request->working_on_day,
            'user_id' => $request->user_id,
            'check_in' => $request->check_in,
            'check_out' => $request->check_out,
        ]);
        toast('Add Check Success', 'success')->width('400px')->position('top-right')->autoClose(3000)->background('#fff');;
        return redirect()->back();
    }
}
