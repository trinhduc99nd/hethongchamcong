<?php

namespace App\Http\Controllers;

use App\Repositories\UserChecks\UserCheckRepositoryInterface;
use RealRashid\SweetAlert\Facades\Alert;

class UserCheckController extends Controller
{
    protected $userCheckRepository;

    public function __construct(UserCheckRepositoryInterface $userCheckRepository)
    {
        $this->middleware(['auth']);
        $this->userCheckRepository = $userCheckRepository;
    }

    public function index()
    {
        $events = $this->userCheckRepository->getListUserCheck();
        $events = json_encode($events);
        return view('timesheet_management.calendar_work.index', compact('events'));
    }

    public function checkin()
    {
        $result = $this->userCheckRepository->checkUserCheckIn();
        if (isset($result)) {
            toast('You have already checked in. Thank you','info')->width('400px')->position('top-right')->autoClose(5000)->background('#fff');;
            return redirect()->back();
        }
        $this->userCheckRepository->create($this->userCheckRepository->getUserCheckIn());
        toast('CheckIn Success','success')->width('400px')->position('top-right')->autoClose(3000)->background('#fff');;
        return redirect()->back();
    }

    public function checkout(): \Illuminate\Http\RedirectResponse
    {
       $result = $this->userCheckRepository->updateUserCheckOut();
        if ($result === 0) {
            toast('You have already checked out. Thank you','info')->width('400px')->position('top-right')->autoClose(5000)->background('#fff');;
        } else {
            toast('CheckOut Success', 'success')->width('400px')->position('top-right')->autoClose(3000);
        }
        return redirect()->back();
    }

}
