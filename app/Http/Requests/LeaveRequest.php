<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LeaveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'start_at' => ['required','date'],
            'end_at' => ['required','date','after_or_equal:start_date'],
            'phone' => ['required','max:10','min:10'],
            'content' => ['required','string','min:5','max:4000'],
        ];
    }
    public function messages(): array
    {
        return [
            'start_at.required' => 'Please choose Start_at',
            'end_at.required' => 'Please choose End_at',
            'start_at.date' => 'Please choose format-date',
            'end_at.date' => 'Please choose format-date',
            'end_at.after_or_equal' => 'Please choose end at after start at',
            'phone.required' => 'Phone not required',
            'content.required' => 'Content not required',
            'content.min' => 'Content min 5 character',
            'content.max' => 'Content max 4000 character',
        ];
    }
}
