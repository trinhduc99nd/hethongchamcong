<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'position' => ['required', 'array'],
            'role' => ['in:staff,admin'],
            'division' => ['in:1,2'],
        ];
    }
    public function messages(): array
    {
        return [
            'first_name.required' => 'Please enter your firstname',
            'last_name.required' => 'Please enter your lastname',
            'email.required' => 'Please enter your email',
            'password.required' => 'Please provide a password',
            'position.required' => 'Please provide a position off user',
            'role' => 'Role only staff or admin',
            'password.min' => 'Your password must be at least 6 characters long',
        ];
    }
}
