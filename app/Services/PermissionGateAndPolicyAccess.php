<?php
namespace App\Services;

use App\User;
use Illuminate\Support\Facades\Gate;

class PermissionGateAndPolicyAccess
{
    public function setGateAndPolicyAccess()
    {
        $this->defineGateUser();
        $this->defineGateLeaveRequest();
    }

    public function defineGateLeaveRequest()
    {
        Gate::define('leave-index','App\Policies\LeaveRequestPolicy@index');
        Gate::define('leave-create','App\Policies\LeaveRequestPolicy@create');
        Gate::define('leave-store','App\Policies\LeaveRequestPolicy@store');
        Gate::define('leave-accept','App\Policies\LeaveRequestPolicy@accept');
        Gate::define('leave-deny','App\Policies\LeaveRequestPolicy@deny');
    }

    public function defineGateUser()
    {
        Gate::define('isAdmin', function(User $user) {
            return $user->role == 'admin';
        });
        Gate::define('isStaff', function(User $user) {
            return $user->role == 'staff';
        });
        Gate::define('editInfo', function(User $user,$id) {
            return $user->id == $id;
        });
        Gate::define('statistic', function(User $user,$id) {
            return ($user->id == $id)|| ($user->role ==='admin');
        });
        Gate::define('user-view','App\Policies\UserPolicy@view');
        Gate::define('user-create','App\Policies\UserPolicy@create');
        Gate::define('user-update','App\Policies\UserPolicy@update');
        Gate::define('user-delete','App\Policies\UserPolicy@delete');
    }
}
