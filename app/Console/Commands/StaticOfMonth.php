<?php

namespace App\Console\Commands;

use App\Repositories\LeaveRequests\LeaveRequestRepositoryInterface;
use App\Repositories\UserChecks\UserCheckRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Console\Command;

class StaticOfMonth extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'statistic:month';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Static list working day, off day in month';
    protected $userCheckRepository;
    protected $leaveRequestRepository;

    /**
     * Create a new command instance.
     *
     * @param UserCheckRepositoryInterface $userCheckRepository
     * @param LeaveRequestRepositoryInterface $leaveRequestRepository
     */

    public function __construct(UserCheckRepositoryInterface $userCheckRepository, LeaveRequestRepositoryInterface $leaveRequestRepository)
    {
        parent::__construct();
        $this->userCheckRepository = $userCheckRepository;
        $this->leaveRequestRepository = $leaveRequestRepository;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
//        $now = Carbon::now()->subMonth();
        $now = Carbon::create('2019','12','10');
        $this->userCheckRepository->insertUserTimeSheet($now);
        $this->leaveRequestRepository->updateLeaveRequestInStatistic($now);
        echo 'task static month success run!';
        return '';
    }
}
