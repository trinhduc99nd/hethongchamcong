<?php

namespace App\Repositories\LeaveRequests;

use App\Repositories\EloquentRepository;
use App\LeaveRequest;
use App\Statistic;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class LeaveRequestRepository extends EloquentRepository implements LeaveRequestRepositoryInterface
{
    /**
     * get model
     * @return string
     */
    public function getModel(): string
    {
        return LeaveRequest::class;
    }

    public function getListLeaveRequestPerson()
    {
        $userId = Auth::user()->id;
        return $this->_model::where('user_id', $userId)->get();
    }

    public function getListLeaveRequestPersonId($id)
    {
        return $this->_model::where('id', $id)->first()->user_id;
    }


    public function insertDataLeaveRequest($request): array
    {
        return array(
            'start_at' => $request->start_at,
            'end_at' => $request->end_at,
            'content' => $request->content,
            'phone' => $request->phone,
            'user_id' => Auth::user()->id
        );
    }

    public function updateLeaveRequestInStatistic($now)
    {
        $userLeaveRequestLists = $this->_model::distinct()->get('user_id');
        foreach ($userLeaveRequestLists as $user) {
            $userChecks = $this->_model::where('user_id', $user->user_id)->where('status', 'approved')
                ->whereRaw("MONTH(`end_at`) = $now->month")
                ->whereRaw("YEAR(`end_at`) = $now->year")->get();
            if (sizeof($userChecks)>0) {
                foreach ($userChecks as $userCheck) {
                    $countDayOff = 0;
                    $countDayOff += $userCheck->getDayTwoDay();
                }
                Statistic::firstOrCreate([
                    'user_id' => $user->user_id,
                    'month' => $now->month,
                    'year' => $now->year,
                    'leave_days_count' => $countDayOff
                ]);
            }
        }
    }

}
