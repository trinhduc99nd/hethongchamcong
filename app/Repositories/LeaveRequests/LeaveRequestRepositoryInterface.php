<?php
namespace App\Repositories\LeaveRequests;

interface LeaveRequestRepositoryInterface
{

    public function insertDataLeaveRequest($request);
    public function getListLeaveRequestPerson();
    public function getListLeaveRequestPersonId($id);
    public function updateLeaveRequestInStatistic($now);
}
