<?php

namespace App\Repositories\Users;

use App\Position;
use App\Repositories\EloquentRepository;
use App\Repositories\Positions\PositionRepositoryInterface;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserRepositoryRepository extends EloquentRepository implements UserRepositoryInterface
{
    /**
     * get model
     * @return string
     */
    public function getModel(): string
    {
        return User::class;
    }

    public function getListAdminUser()
    {
        return $this->_model::where('role', 'admin')->get();
    }

    public function getNameUser()
    {
        $userId = Auth::user()->id;
        return $this->_model::where('id', $userId)->first()->getFullNameAttribute();
    }

    public function getUser($userId)
    {
        return $this->_model::where('id', $userId)->first();
    }

    public function insertDataUser($request): array
    {
        return [
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'division' => $request->division,
            'role' => $request->role,
            'password' => Hash::make($request->password)
        ];
    }

    public function updateDataUser($request): array
    {
        return [
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'division' => $request->division,
            'role' => $request->role,
        ];
    }

    public function updateDataUserInfo($request): array
    {
        if ($request->file('file')) {
            $imagePath = $request->file('file');
            $imageName = $imagePath->getClientOriginalName();
            $path = $request->file('file')->storeAs('avatar', $imageName, 'public');
            $path = '/storage/'.$path;
            return [
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'phone' => $request->phone,
                'gender' => $request->gender,
                'birthday' => $request->birthday,
                'profile_image' => $path
            ];
        }
        return [
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'phone' => $request->phone,
            'gender' => $request->gender,
            'birthday' => $request->birthday,
        ];

    }



}
