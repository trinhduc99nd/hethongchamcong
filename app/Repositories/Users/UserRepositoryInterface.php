<?php
namespace App\Repositories\Users;


interface UserRepositoryInterface
{
//    /**
//     * Get 5 posts hot in a month the last
//     * @return mixed
//     */
    public function insertDataUser($request);
    public function updateDataUser($request);
    public function updateDataUserInfo($request);
    public function getListAdminUser();
    public function getNameUser();
    public function getUser($userId);
}
