<?php
namespace App\Repositories\Positions;

interface PositionRepositoryInterface
{
    public function firstOrCreatePosition($positionUser);
}
