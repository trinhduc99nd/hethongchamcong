<?php

namespace App\Repositories\Positions
;

use App\Position;
use App\Repositories\EloquentRepository;

class PositionEloquentRepository extends EloquentRepository implements PositionRepositoryInterface
{
    /**
     * get model
     * @return string
     */
    public function getModel()
    {
        return Position::class;
    }

    /**
     * get model
     * @param $positionUser
     * @return array
     */
    public function firstOrCreatePosition($positionUser)
    {
        return $this->_model->firstOrCreate(['name' => $positionUser]);
    }

}
