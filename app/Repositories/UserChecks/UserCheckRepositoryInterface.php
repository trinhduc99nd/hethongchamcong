<?php
namespace App\Repositories\UserChecks;

interface UserCheckRepositoryInterface
{
    /**
     * Get 5 posts hot in a month the last
     * @return mixed
     */
    public function updateUserCheckOut();
    public function updateUserCheckOutId($id);
    public function checkUserCheckIn();
    public function getUserCheckIn();
    public function getListUserCheck();
    public function getListWorkingOfDayUser();
    public function insertUserTimeSheet($now);
    public function formatTime($t,$f=':');
    public function getListUser();
    public function updateTimeSheetOfUser($request);
    public function getTimeSheetUserInMonth($month,$year,$userId);
}
