<?php

namespace App\Repositories\UserChecks;

use App\LeaveRequest;
use App\Repositories\EloquentRepository;
use App\Statistic;
use App\UserCheck;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class UserCheckEloquentRepository extends EloquentRepository implements UserCheckRepositoryInterface
{
    /**
     * get model
     * @return string
     */
    public function getModel()
    {
        return UserCheck::class;
    }

    /**
     * @return mixed
     */
    public function updateUserCheckOut()
    {
        $workingOnDay = Carbon::now();
        return $this->_model::whereNull('check_out')
            ->where('working_on_day', $workingOnDay->toDateString())
            ->update(['check_out' => $workingOnDay->toTimeString()]);
    }

    /**
     * @return mixed
     */
    public function checkUserCheckIn()
    {
        $userId = Auth::user()->id;
        $workingOnDay = \Carbon\Carbon::now();
        return $this->_model::whereNotNull('check_in')
            ->where([['working_on_day', '=', $workingOnDay->toDateString()], ['user_id', '=', $userId]])->first();
    }

    public function updateTimeSheetOfUser($request)
    {
        return $this->_model::where([['working_on_day', '=', $request->working_on_day], ['user_id', '=', $request->user_id]])->first();
    }


    /**
     * @return mixed
     */
    public function getUserCheckIn()
    {
        $workingOnDay = \Carbon\Carbon::now();
        return array(
            'working_on_day' => $workingOnDay->toDateString(),
            'check_in' => $workingOnDay->toTimeString(),
            'check_out' => null,
            'user_id' => Auth::user()->id
        );
    }

    public function getListWorkingOfDayUser()
    {
        $userId = Auth::user()->id;
        return $this->_model::where('user_id', '=', $userId)->get();
    }


    /**
     * @return mixed
     */
    public function getListUserCheck()
    {
        $events = [];
        $timeTrueCheckIn = "08:30:00";
        $timeTrueCheckOut = "17:30:00";
        $userId = Auth::user()->id;
        $userChecks = $this->_model::where('user_id', '=', $userId)->get();
        $leaveRequests = LeaveRequest::where([
            'user_id' => $userId,
            'status' => 'approved'
        ])->get();

        foreach ($userChecks as $userCheck) {
            if (strtotime($userCheck->check_in) >= strtotime($timeTrueCheckIn)) {
                $events [] = [
                    'id' => $userCheck->id,
                    'title' => 'check_in',
                    'start' => \Carbon\Carbon::parse($userCheck->working_on_day . ' ' . $userCheck->check_in)->toDateTimeString(),
                    'backgroundColor' => '#d43b1e'
                ];
            } else {
                $events [] = [
                    'id' => $userCheck->id,
                    'title' => 'check_in',
                    'start' => Carbon::parse($userCheck->working_on_day . ' ' . $userCheck->check_in)->toDateTimeString(),
                ];
            }
            if ($userCheck->check_out) {
                if (strtotime($userCheck->check_out) <= strtotime($timeTrueCheckOut)) {
                    array_push($events, [
                        'id' => $userCheck->id,
                        'title' => 'check_out',
                        'start' => Carbon::parse($userCheck->working_on_day . ' ' . $userCheck->check_out)->toDateTimeString(),
                        'backgroundColor' => '#d43b1e'
                    ]);
                } else {
                    array_push($events, [
                        'id' => $userCheck->id,
                        'title' => 'check_out',
                        'start' => Carbon::parse($userCheck->working_on_day . ' ' . $userCheck->check_out)->toDateTimeString(),
                    ]);
                }
            }
        }
        if (sizeof($leaveRequests) > 0) {
            foreach ($leaveRequests as $leaveRequest) {
                $events [] = [
                    'id' => $leaveRequest->id,
                    'title' => 'LeaveRequest Off',
                    'start' => Carbon::parse($leaveRequest->start_at)->toDateTimeString(),
                    'end' => Carbon::parse($leaveRequest->end_at)->toDateTimeString(),
                    'backgroundColor' => '#253738'
                ];
            }
        }
        return $events;
    }

    public function updateUserCheckOutId($id)
    {
        $this->_model->where('id',$id)->whereNull('check_out')->update([
            'check_out' => '17:30:00'
        ]);
    }

    public function formatTime($t, $f = ':'): string
    {
        return sprintf("%02d%s%02d%s%02d", floor($t / 3600), $f, ($t / 60) % 60, $f, $t % 60);
    }

    public function insertUserTimeSheet($now)
    {
        $users = $this->_model::distinct()->get('user_id');
        foreach ($users as $user) {
            $userChecks = $this->_model::where('user_id', $user->user_id)
                ->whereNotNull('check_out')
                ->whereRaw("MONTH(`working_on_day`) = $now->month")
                ->whereRaw("YEAR(`working_on_day`) = $now->year");
            if (sizeof($userChecks->get()) > 0) {
                $countHour = 0;
                foreach ($userChecks->get() as $userCheck) {
                    $countHour += $userCheck->countHourInDay();
                }
                $countHour = $this->formatTime($countHour);
                Statistic::firstOrCreate([
                    'user_id' => $user->user_id,
                    'month' => $now->month,
                    'year' => $now->year,
                    'working_days_count' => $userChecks->count(),
                    'working_hours_count' => $countHour
                ]);
            }
        }
    }

    public function getTimeSheetUserInMonth($month,$year,$userId)
    {
        return $this->_model::where('user_id', $userId)
            ->whereRaw("MONTH(`working_on_day`) = $month")
            ->whereRaw("YEAR(`working_on_day`) = $year")->get();
    }

    public function getListUser()
    {
        return $this->_model->orderby('created_at', 'desc')->get();
    }
}
