<?php
namespace App\Repositories\Statics;

interface StaticCheckRepositoryInterface
{
    public function getYearDistinct();
    public function getMonthDistinct($year);
    public function getData( $year, $month);
    public function getDataUser( $id,$year, $month);
}
