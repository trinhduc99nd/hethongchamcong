<?php

namespace App\Repositories\Statics;

use App\Repositories\EloquentRepository;
use App\Statistic;
use Illuminate\Support\Facades\Auth;

class StaticEloquentRepository extends EloquentRepository implements StaticCheckRepositoryInterface
{
    /**
     * get model
     * @return string
     */
    public function getModel()
    {
        return Statistic::class;
    }

    public function getYearDistinct()
    {
        return $this->_model::distinct()->get('year');
    }

    public function getMonthDistinct($year)
    {
        return $this->_model::where('year',$year)
            ->distinct()->get(["month"]);
    }

    public function getData( $year, $month)
    {
        $user = Auth::user();
        if($user->role === 'admin') {
            return $this->_model::where([
                'year' => $year,
                'month' => $month,
            ])->get()->load('users');
        } else { return $this->_model::where([
            'user_id' => $user->id,
            'year' => $year,
            'month' => $month,
        ])->get()->load('users');
        }
    }

    public function getDataUser($id, $year, $month)
    {
        return $this->_model::where([
            'year' => $year,
            'user_id' => $id,
            'month' => $month,
        ])->first();
    }
}
