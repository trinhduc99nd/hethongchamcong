<?php

return [
    'calendar'=>'Lịch Làm Việc',
    'historyTimeSheet' => 'Nhật ký chấm công',
    'person' => 'Cá nhân',
    'people' => 'Mọi người',
    'list_request' => 'Danh sách Yêu cầu',
    'list_request_off' => 'Yêu cầu xin nghỉ',
    'statistic' => 'Thống Kê',
    'list_user' => 'Danh sách Tài Khoản',
];
