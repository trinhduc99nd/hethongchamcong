<?php

return [
    'welcome'=>'Xin Chào, :Name',
    'info' => 'Thông tin cá nhân',
    'change_password' => 'Thay đổi mật khẩu',
    'logout' => 'Đăng Xuất',
    'notification' =>'Thông báo'
];
