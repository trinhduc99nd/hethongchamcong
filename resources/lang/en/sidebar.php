<?php

return [
    'calendar'=>'TimeSheet Working',
    'historyTimeSheet' => 'History TimeSheet',
    'person' => 'Person',
    'people' => 'People',
    'list_request' => 'Leave Request',
    'list_request_off' => 'Leave Request Off',
    'statistic' => 'Statistic',
    'list_user' => 'List User',
];
