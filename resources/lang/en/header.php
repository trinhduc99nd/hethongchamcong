<?php

return [
    'welcome'=>'Hi, :Name',
    'info' => 'Info Person',
    'change_password' => 'Change Password',
    'logout' => 'LogOut',
    'notification' =>'Notifications'
];
