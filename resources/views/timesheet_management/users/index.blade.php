@section('css_timesheet_management')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.css">
@endsection
@section('title_timesheet_management')
    {{__('config.users_index.title')}}
@endsection
@section('active_listUser','active')
@section('js_timesheet_management')
    <script type="text/javascript" charset="utf8"
            src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.js"></script>
    <script>
        $(document).ready(function () {
            $('#tableUser').DataTable();
        });
        $(function () {
            $(document).on('click', '.action_delete', actionDelete);
        });

        function actionDelete(event) {
            event.preventDefault();
            let urlRequest = $(this).data('url');
            let that = $(this);
            Swal.fire({
                title: 'Are you sure?',
                text: "User count will be deleted !",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Deleted!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: urlRequest,
                        success: function (data) {
                            if (data.code === 200) {
                                that.parent().parent().remove();
                                Swal.fire(
                                    {
                                        icon: 'success',
                                        title: 'Delete Success!',
                                        text: 'User deleted.',
                                        footer: 'success'
                                    }
                                )
                            }
                        }, error: function () {
                            Swal.fire({
                                icon: 'error',
                                title: 'Delete failed!',
                                footer: 'Failed!',
                            })
                        },
                    });
                }
            });
        }
    </script>
@endsection

@extends('layouts.timesheet_management')
@section('content_timesheet_management')
    <div class="main-content container-fluid">
        <div class="page-title ">
            <h3 class="float-left"> {{__('config.users_index.header')}}</h3>
            <a href="{{asset(route('users.add'))}}" class="btn btn-success  mt-2 float-right"
               id="newUser">{{__('config.users_index.new_user')}}</a>
        </div>
        <br><br><br>
        @if(!empty($leave_requests))
            <div class='panel-container'>
                <div class='panel-content' id='requests'>
                    <hr>
                    <div class="text-center">
                        <h1>{{__('config.users_index.no_data')}}</h1>
                    </div>
                </div>
            </div>
        @else
            <section class="section">
                <table class="display table" id="tableUser">
                    <thead>
                    <tr class="text-center">
                        <th scope="col" width="5%">{{__('config.users_index.table.id')}}</th>
                        <th scope="col" width="15%">{{__('config.users_index.table.username')}}</th>
                        <th scope="col" width="15%">{{__('config.users_index.table.email')}}</th>
                        <th scope="col" width="25%">{{__('config.users_index.table.position')}}</th>
                        <th scope="col" width="10%">{{__('config.users_index.table.division')}}</th>
                        <th scope="col" width="10%">{{__('config.users_index.table.role')}}</th>
                        <th scope="col" width="20%">{{__('config.users_index.table.action')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <th scope="row">{{$user->id}}</th>
                            <td>{{$user->getFullNameAttribute()}}</td>
                            <td>{{$user->email}}</td>
                            <td>
                                    @for($i = 0; $i < count($user->positions); $i++)
                                        @if($i == count($user->positions) - 1)
                                            {{$user->positions[$i]->name}}
                                        @else
                                            {{$user->positions[$i]->name}},
                                        @endif
                                    @endfor
                            </td>
                            <td>{{$user->division}}</td>
                            <td>{{$user->role}}</td>
                            <td>
                                <a href="{{route('users.edit',['id' => $user->id])}}" title="edit user"
                                   class="btn btn-info"><i class="fas fa-user-edit"></i></a>
                                @unless($user->role ==='admin')
                                    <a data-url="{{route('users.delete',['id' => $user->id])}}" title="delete user"
                                       class="btn action_delete btn-danger"><i class="fas fa-user-minus"></i></a>
                                @endunless
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </section>
        @endif
    </div>
@endsection
