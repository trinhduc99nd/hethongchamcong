@section('css_timesheet_management')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet"/>
@endsection
@section('title_timesheet_management')
    {{__('config.users_add.title')}}
@endsection
@section('active_listUser','active')
@section('js_timesheet_management')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
    <script src="{{asset('js/user.js')}}"></script>
    <script>
        $(".js-example-tags").select2({
            placeholder: "Select a position",
            allowClear: true,
            tags: true,
        });
    </script>
@endsection
@extends('layouts.timesheet_management')
@section('content_timesheet_management')
    <div class="main-content container-fluid">
        <div class="page-title ">
            <h3 class="float-left">{{__('config.users_add.header')}}</h3>
        </div>
        <br><br><br>
        <section class="section">
            <form id="addUser" class="col-6" action="{{route('users.create')}}" method="post">
                @csrf
                <div class="row">
                    <div class="col">
                        <label for="firstName">{{__('config.users_add.first_name')}}</label>
                        <input type="text" id="firstName" class="form-control @error('first_name') is-invalid @enderror"
                               name="first_name"
                               value="{{old('first_name')}}"
                               placeholder="Enter First Name">
                        @error('first_name')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="col">
                        <label for="lastName">{{__('config.users_add.last_name')}}</label>
                        <input type="text" id="lastName" class="form-control @error('last_name') is-invalid @enderror"
                               name="last_name"
                               value="{{old('last_name')}}"
                               placeholder="Enter Last Name">
                        @error('last_name')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col">
                        <label for="email">{{__('config.users_add.email')}}</label>
                        <input type="email" class="form-control @error('email') is-invalid @enderror" id="email"
                               name="email"
                               value="{{old('email')}}"
                               placeholder="Enter email">
                        @error('email')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col">
                        <label for="password">{{__('config.users_add.password')}}</label>
                        <input type="password" class="form-control @error('password') is-invalid @enderror"
                               id="password" name="password"
                               placeholder="Password">
                        @error('password')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col">
                        <label for="password_confirmation">{{__('config.users_add.confirm_password')}}</label>
                        <input type="password" name="password_confirmation" class="form-control" id="password_confirmation"
                               placeholder="Confirm password">
                    </div>
                </div>
                <div class="row mt-2">
                    <label for="position">{{__('config.users_add.position')}}</label><br>
                    <select class="form-control form-group  js-example-tags @error('position') is-invalid @enderror"
                            id="position" name="position[]"
                            multiple="multiple" size="1" required title="Please add or choose one position">
                        <option value="" disabled>Chọn vị trí</option>
                        @foreach($positions as $position)
                            <option value="{{$position->name}}">{{$position->name}}</option>
                        @endforeach
                    </select>
                    @error('position')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <fieldset class="form-group mt-3">
                    <legend class="col-form-label col-sm-2 pt-0">{{__('config.users_add.division')}}</legend>
                    <div class="col-8">
                        <div class="form-check form-check-inline mr-5">
                            <input class="form-check-input @error('division') is-invalid @enderror" type="radio" checked name="division"
                                   id="division1"
                                   value="1">
                            <label class="form-check-label" for="division1">{{__('config.users_add.one')}}</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" id="division2" name="division"
                                   value="2">
                            <label class="form-check-label" for="division2">{{__('config.users_add.two')}}</label>
                        </div>
                        @error('division')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </fieldset>
                <fieldset class="form-group mt-3">
                    <legend class="col-form-label col-sm-2 pt-0">{{__('config.users_add.role')}}</legend>
                    <div class="col-8">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input @error('role') is-invalid @enderror" type="radio" checked name="role"
                                   id="role1"
                                   value="staff">
                            <label class="form-check-label" for="role1">{{__('config.users_add.staff')}}</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" id="role2" name="role"
                                   value="admin">
                            <label class="form-check-label" for="role2">{{__('config.users_add.admin')}}</label>
                        </div>
                        @error('role')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </fieldset>
                <button type="submit" class="btn btn-primary mt-3">{{__('config.users_add.submit')}}</button>
            </form>
        </section>
    </div>
@endsection

