@section('css_timesheet_management')
@endsection
@section('title_timesheet_management')
    {{__('config.users_info.title',['Name' => $user->getFullNameAttribute()])}}
@endsection
@section('js_timesheet_management')
    <script>

    </script>
@endsection
@extends('layouts.timesheet_management')
@section('content_timesheet_management')
    <div class="main-content container-fluid">
        <div class="page-title ">
            <h3 class="float-left">{{__('config.users_info.header',['Name' => $user->getFullNameAttribute()])}}</h3>
        </div>
        <br><br><br>
        <div class="row">
            <div>
                <label for="recipient-name"
                       class="col-form-label font-bold color-dark w-25">{{__('config.users_info.fullname')}}</label>
                <span
                    class="color-dark">{{$user->getFullNameAttribute() ??"null"}}</span>
            </div>
            <div>
                <label for="recipient-name"
                       class="col-form-label font-bold color-dark w-25">{{__('config.users_info.gender')}}</label>
                <span
                    class="color-dark">{{$user->gender ??"null"}}</span>
            </div>
            <div>
                <label for="recipient-name"
                       class="col-form-label font-bold color-dark w-25">{{__('config.users_info.phone')}}</label>
                <span
                    class="color-dark">{{$user->phone ??"null"}}</span>
            </div>
            <div>
                <label for="recipient-name"
                       class="col-form-label font-bold color-dark w-25">{{__('config.users_info.email')}}</label>
                <span
                    class="color-dark">{{$user->email ??"null"}}</span>
            </div>
            <div>
                <label for="recipient-name"
                       class="col-form-label font-bold color-dark w-25">{{__('config.users_info.birthday')}}</label>
                <span
                    class="color-dark">{{$user->birthday ??"null"}}</span>
            </div>
            <div>
                <label for="recipient-name"
                       class="col-form-label font-bold color-dark w-25">{{__('config.users_info.division')}}</label>
                <span
                    class="color-dark">{{$user->division ??"null"}}</span>
            </div>
            <div>
                <label for="recipient-name"
                       class="col-form-label font-bold color-dark w-25">{{__('config.users_info.position')}}</label>
                <span class="color-dark">
                      @for($i = 0; $i < count($user->positions); $i++)
                        @if($i == count($user->positions) - 1)
                            {{$user->positions[$i]->name}}
                        @else
                            {{$user->positions[$i]->name}},
                        @endif
                    @endfor
                </span>
            </div>
            <div>
                <label for="recipient-name"
                       class="col-form-label font-bold color-dark w-25">{{__('config.users_info.image')}}</label>
                <img class="rounded-circle mt-5" alt="" width="150px" height="150px"
                     src="{{asset($user->profile_image ??"admin/assets/images/avatar.png")}}">
            </div>

        </div>
        <a href="{{asset(route('users.editInfo',['id' =>$user->id]))}}" class="btn btn-info"
           id="newUser">{{__('config.users_info.edit')}}</a>
    </div>
@endsection

