@section('css_timesheet_management')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet"/>
@endsection
@section('title_timesheet_management')
    {{__('config.users_info_edit.title',['Name' => $user->getFullNameAttribute()])}}
@endsection
@section('active_listUser','active')
@section('js_timesheet_management')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
    <script src="{{asset('js/user.js')}}"></script>
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                const reader = new FileReader();
                reader.onload = function (e) {
                    $('#profile-img-tag').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#profile-img").change(function () {
            readURL(this);
        });
    </script>
@endsection
@extends('layouts.timesheet_management')
@section('content_timesheet_management')
    <div class="main-content container-fluid">
        <div class="page-title ">
            <h3 class="float-left"> {{__('config.users_info_edit.header',['Name' => $user->getFullNameAttribute()])}}</h3>
        </div>
        <br><br><br>
        <section class="section">
            <form id="addUser" enctype="multipart/form-data" class="col-6" action="{{route('users.updateInfo',['id' =>$user->id])}}" method="post">
                @csrf
                <div class="row">
                    <div class="col">
                        <label for="firstName"> {{__('config.users_info_edit.first_name')}}</label>
                        <input type="text" id="firstName" class="form-control @error('first_name') is-invalid @enderror"
                               name="first_name"
                               value="{{$user->first_name}}"
                               placeholder="Enter First Name">
                        @error('first_name')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="col">
                        <label for="lastName">{{__('config.users_info_edit.last_name')}}</label>
                        <input type="text" id="lastName" class="form-control @error('last_name') is-invalid @enderror"
                               name="last_name"
                               value="{{$user->last_name}}"
                               placeholder="Enter Last Name">
                        @error('last_name')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col">
                        <label for="email">{{__('config.users_info_edit.email')}}</label>
                        <input type="email" class="form-control " id="email"
                               value="{{$user->email}}"
                                readonly>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-6">
                        <label for="phone">{{__('config.users_info_edit.phone')}}</label>
                        <input type="tel" class="form-control @error('phone') is-invalid @enderror" id="phone"
                               name="phone" pattern="^\+?(?:[0-9]??).{9,12}[0-9]$" required
                               value="{{$user->phone}}">
                        @error('phone')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-6">
                        <label for="birthday">{{__('config.users_info_edit.birthday')}}</label>
                        <input type="date" class="form-control @error('birthday') is-invalid @enderror" id="birthday"
                               name="birthday"
                               value="{{$user->birthday}}">
                        @error('birthday')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <fieldset class="form-group mt-3">
                    <legend class="col-form-label col-sm-2 pt-0">{{__('config.users_info_edit.gender')}}</legend>
                    <div class="col-8">
                            <div class="form-check form-check-inline mr-5">
                                <input class="form-check-input @error('division') is-invalid @enderror" type="radio"
                                        name="gender" {{($user->gender ==='male' )?"checked":""}}
                                       id="division1"
                                       value="male">
                                <label class="form-check-label"
                                       for="division1">{{__('config.users_info_edit.male')}}</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" id="division2"  name="gender"
                                       value="female" {{($user->gender ==='female' )?"checked":""}}>
                                <label class="form-check-label"
                                       for="division2">{{__('config.users_info_edit.female')}}</label>
                            </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-6">
                            <label for="birthday">{{__('config.users_info_edit.avatar')}}</label>
                            <input type="file" name="file" id="profile-img">
                            <br>
                            <img src="{{asset($user->profile_image??"admin/assets/images/avatar.png")}}" id="profile-img-tag"
                                  class="rounded-circle mt-3" alt="" width="150px" height="150px"/>
                        </div>
                    </div>
                </fieldset>
                <button type="submit" class="btn btn-primary mt-3">{{__('config.users_info_edit.update')}}</button>
            </form>
        </section>
    </div>
@endsection

