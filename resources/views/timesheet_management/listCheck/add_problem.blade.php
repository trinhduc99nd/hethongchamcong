@section('css_timesheet_management')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <style>
        .ck-editor__editable_inline {
            min-height: 200px;
        }

        .ck.ck-editor__editable > .ck-placeholder::before {
            color: #d21714;
            font-family: Georgia, serif;
        }
    </style>
@endsection
@section('title_timesheet_management')
    {{__('config.list_check.add_problem.title')}}
@endsection
@section('active_listUser','active')
@section('js_timesheet_management')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://cdn.ckeditor.com/ckeditor5/24.0.0/classic/ckeditor.js"></script>
    <script type="text/javascript">
        const ourDate = new Date();
        $('#day').datepicker({ dateFormat: 'yy-mm-dd'}).datepicker("setDate", ourDate)
            .datepicker( "option", "maxDate",ourDate);
        ClassicEditor
            .create(document.querySelector('#content'), {
                placeholder: 'Type the content reason here!'
            })
            .then(editor => {
            })
            .catch(error => {
            });
    </script>
@endsection
@extends('layouts.timesheet_management')
@section('content_timesheet_management')
    <div class="main-content container-fluid">
        <div class="page-title ">
            <h3 class="float-left"> {{__('config.list_check.add_problem.header')}}</h3>
        </div>
        <br><br><br>
        <section class="section">
            <form id="addUser" class="col-12" action="{{route('person.storeProblem')}}" method="post">
                @csrf
                <div class="row">
                    <div class="col-3">
                        <label for="days"> {{__('config.list_check.add_problem.choose_day')}}</label>
                        <input type="text" id="day"
                               class="form-control @error('day') is-invalid @enderror datetimepicker-input"
                               data-toggle="datetimepicker" data-target="#day"
                               name="day"
                               value="{{old('day')}}">
                        @error('day')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-8">
                        <label for="email">{{__('config.list_check.add_problem.content')}}</label>
                        <textarea class="form-control @error('phone') is-invalid @enderror" aria-label="With textarea"
                                  id="content" name="content"></textarea>
                        @error('content')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <button type="submit" class="btn btn-primary mt-3">{{__('config.list_check.add_problem.send')}}</button>
            </form>
        </section>
    </div>
@endsection

