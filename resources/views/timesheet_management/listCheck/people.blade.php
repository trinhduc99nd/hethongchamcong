@section('css_timesheet_management')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.css">
@endsection
@section('title_timesheet_management')
    {{__('config.list_check.title')}}
@endsection
@section('active_history','active')
@section('js_timesheet_management')
    <script type="text/javascript" charset="utf8"
            src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.js"></script>
    <script>
        $(document).ready(function () {
            $('#tableUser').DataTable({
                "order": [[ 0, "desc" ]]
            });
        });
    </script>
@endsection
@extends('layouts.timesheet_management')
@section('content_timesheet_management')
    <div class="main-content container-fluid">
        <div class="page-title ">
            <h3 class="float-left">{{__('config.list_check.header_people')}}</h3>
                <a href="{{asset(route('people.viewCheck'))}}" class="btn btn-success  mt-2 float-right"
                   id="newUser">+{{__('config.list_check.add_check.header')}}</a>
        </div>
        <br><br><br>
        @if(empty($listCheckUsers))
            <div class='panel-container'>
                <div class='panel-content' id='requests'>
                    <hr>
                    <div class="text-center">
                        <h1>{{__('config.list_check.no_data')}}</h1>
                    </div>
                </div>
            </div>
        @else
            <section class="section">
                <table class="display table" id="tableUser">
                    <thead>
                    <tr class="text-center">
                        <th scope="col" width="5%">{{__('config.list_check.table.id')}}</th>
                        <th scope="col" width="20%">{{__('config.list_check.table.username')}}</th>
                        <th scope="col" width="20%">{{__('config.list_check.table.day')}}</th>
                        <th scope="col" width="20%">{{__('config.list_check.table.checkin')}}</th>
                        <th scope="col" width="15%">{{__('config.list_check.table.checkout')}}</th>
                        <th scope="col" width="15%">{{__('config.list_check.table.working_hour')}}</th>
                        <th scope="col" width="5%">{{__('config.list_check.table.action')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($listCheckUsers as $listCheckUser)
                        <tr class="text-center">
                            <td scope="row">{{$listCheckUser->id}}</td>
                            <td>{{$listCheckUser->user->getFullNameAttribute()}}</td>
                            <td>{{$listCheckUser->working_on_day}}</td>
                            @if(strtotime($listCheckUser->check_in) >= strtotime("08:30:00"))
                                <td>
                                    <span class="color-danger">{{$listCheckUser->check_in}}</span>
                                </td>
                            @else
                                <td>
                                    <span class="color-primary">{{$listCheckUser->check_in}}</span>
                                </td>
                            @endif
                            @if(strtotime($listCheckUser->check_out) <= strtotime("17:30:00"))
                                <td>
                                    <span class="color-danger">{{$listCheckUser->check_out}}</span>
                                </td>
                            @else
                                <td>
                                    <span class="color-primary">{{$listCheckUser->check_out}}</span>
                                </td>
                            @endif
                            <td>
                                @unless(!$listCheckUser->check_out)
                                    @if(strtotime((string)gmdate('H:i:s', $listCheckUser->countHourInDay())) <= strtotime("9:00:00"))
                                        <span class="color-danger"> {{(string)gmdate('H:i:s', $listCheckUser->countHourInDay())}}</span>
                                    @else
                                        <span class="color-primary"> {{(string)gmdate('H:i:s', $listCheckUser->countHourInDay())}}</span>
                                    @endif
                                @endunless
                            </td>
                            <td>
                                @if(!$listCheckUser->check_out)
                                    <a href="{{route('people.updateCheckOut',['id' => $listCheckUser->id])}}" title="update checkout in user"
                                       class="btn btn-info" onclick="return confirm('Do you accept check out for user ?') ;"><i class="fas fa-upload"></i></a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </section>
        @endif
    </div>
@endsection
