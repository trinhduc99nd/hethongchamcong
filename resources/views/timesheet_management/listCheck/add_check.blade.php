@section('css_timesheet_management')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
@endsection
@section('title_timesheet_management')
    {{__('config.list_check.add_check.title')}}
@endsection
@section('active_listUser','active')
@section('js_timesheet_management')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
    <script type="text/javascript">
        const ourDate = new Date();
        $('#day').datepicker({dateFormat: 'yy-mm-dd'}).datepicker("setDate", ourDate)
            .datepicker("option", "maxDate", ourDate);
        $('#checkin').timepicker({
            'minTime': '8:00',
            'maxTime': '12:00',
            'showDuration': true,
            timeFormat: 'HH:mm:ss',
        });
        $('#checkout').timepicker({
            'minTime': '13:00',
            'maxTime': '22:00',
            'showDuration': true,
            timeFormat: 'HH:mm:ss',
        });
    </script>
@endsection
@extends('layouts.timesheet_management')
@section('content_timesheet_management')
    <div class="main-content container-fluid">
        <div class="page-title ">
            <h3 class="float-left">{{__('config.list_check.add_check.header')}}</h3>
        </div>
        <br><br><br>
        <section class="section">
            <form id="addUser" class="col-12" action="{{route('people.storeCheck')}}" method="post">
                @csrf
                <div class="row">
                    <div class="col-3">
                        <label for="days">{{__('config.list_check.add_check.choose_user')}}</label>
                        <select class="form-control form-group  @error('user') is-invalid @enderror"
                                id="user" name="user_id"
                                required title="Please add or choose one user">
                            <option value="{{old('user_id')}}" disabled
                                    selected>{{__('config.list_check.add_check.choose_user')}}</option>
                            @foreach($users as $user)
                                <option value="{{$user->id}}">{{$user->getFullNameAttribute()}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-3">
                        <label for="working_on_day">{{__('config.list_check.add_check.choose_day')}}</label>
                        <input type="text" id="day"
                               class="form-control @error('working_on_day') is-invalid @enderror datetimepicker-input"
                               data-toggle="datetimepicker" data-target="#day"
                               name="working_on_day" required title="Please add or choose one day"
                               value="{{old('working_on_day')}}">
                        @error('working_on_day')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col-3">
                        <label for="days">{{__('config.list_check.add_check.check_in')}}</label>
                        <input type="text" id="checkin"
                               class="form-control @error('checkin') is-invalid @enderror datetimepicker-input"
                               data-toggle="datetimepicker" data-target="#checkin" placeholder="choose hour check in"
                               name="check_in" required title="Please add or choose one hour check out"
                               value="{{old('check_in')}}">
                        @error('day')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="col-3">
                        <label for="days">{{__('config.list_check.add_check.check_out')}}</label>
                        <input type="text" id="checkout"
                               class="form-control @error('checkout') is-invalid @enderror datetimepicker-input"
                               data-toggle="datetimepicker" data-target="#checkout" placeholder="choose hour check out"
                               name="check_out" required title="Please add or choose one hour check out"
                               value="{{old('check_out')}}">
                        @error('day')
                        <div class="alert alert-danger">{{$message}}</div>
                        @enderror
                    </div>
                </div>
                <button type="submit" class="btn btn-primary mt-3">Send</button>
            </form>
        </section>
    </div>
@endsection

