@section('css_timesheet_management')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.css">
@endsection
@section('title_timesheet_management')
    {{__('config.list_check.header_person',['Name' => $userName])}}
@endsection
@section('active_history','active')
@section('js_timesheet_management')
    <script type="text/javascript" charset="utf8"
            src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.js"></script>
    <script>
        $(document).ready(function () {
            $('#tableUser').DataTable({
                "order": [[0, "desc"]]
            });
        });
    </script>
@endsection
@extends('layouts.timesheet_management')
@section('content_timesheet_management')
    <div class="main-content container-fluid">
        <div class="page-title ">
            <h3 class="float-left"> {{__('config.list_check.header_person',['Name' => $userName])}}</h3>
            @can('isStaff')
                <a href="{{asset(route('person.addProblem'))}}" class="btn btn-success  mt-2 float-right"
                   id="newUser">+
                    {{__('config.list_check.add')}}</a>
            @endcan
        </div>
        <br><br><br>
        @if(empty($listCheckPersons))
            <div class='panel-container'>
                <div class='panel-content' id='requests'>
                    <hr>
                    <div class="text-center">
                        <h1> {{__('config.list_check.no_data')}}</h1>
                    </div>
                </div>
            </div>
        @else
            <section class="section">
                <table class="display table" id="tableUser">
                    <thead>
                    <tr class="text-center">
                        <th scope="col" width="10%">{{__('config.list_check.table.id')}}</th>
                        <th scope="col" width="25%">{{__('config.list_check.table.day')}}</th>
                        <th scope="col" width="25%">{{__('config.list_check.table.checkin')}}</th>
                        <th scope="col" width="25%">{{__('config.list_check.table.checkout')}}</th>
                        <th scope="col" width="15%">{{__('config.list_check.table.working_hour')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($listCheckPersons as $listCheckUser)
                        <tr class="text-center">
                            <td scope="row">{{$listCheckUser->id}}</td>
                            <td>{{$listCheckUser->working_on_day}}</td>
                            @if(strtotime($listCheckUser->check_in) >= strtotime("08:30:00"))
                                <td>
                                    <span class="color-danger">{{$listCheckUser->check_in}}</span>
                                </td>
                            @else
                                <td>
                                    <span class="color-primary">{{$listCheckUser->check_in}}</span>
                                </td>
                            @endif
                            <td>
                                @if(strtotime($listCheckUser->check_out) <= strtotime("17:30:00"))
                                    <span class="color-danger">{{$listCheckUser->check_out}}</span>
                                @else
                                    <span class="color-primary">{{$listCheckUser->check_out}}</span>
                                @endif
                            </td>
                            <td>
                                @unless(!$listCheckUser->check_out)
                                    @if(strtotime((string)gmdate('H:i:s', $listCheckUser->countHourInDay())) <= strtotime("9:00:00"))
                                        <span class="color-danger"> {{(string)gmdate('H:i:s', $listCheckUser->countHourInDay())}}</span>
                                    @else
                                        <span class="color-primary"> {{(string)gmdate('H:i:s', $listCheckUser->countHourInDay())}}</span>
                                    @endif
                                @endunless
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </section>
        @endif
    </div>
@endsection
