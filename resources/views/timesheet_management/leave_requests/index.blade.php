@section('css_timesheet_management')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
@endsection
@section('title_timesheet_management')
    {{__('config.leave_requests.title')}}
@endsection
@section('active_leaveRequest','active')
@section('js_timesheet_management')
    <script type="text/javascript" charset="utf8"
            src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.js"></script>
    <script>
        $(document).ready(function () {
            $('#tableLeaveRequest').DataTable({
                "order": [[ 0, "desc" ]]
            });
        });
    </script>
@endsection
@extends('layouts.timesheet_management')
@section('content_timesheet_management')
    <div class="main-content container-fluid">
        <div class="page-title ">
            <h3 class="float-left">{{__('config.leave_requests.header')}}</h3>
            @can('isStaff')
                <a href="{{asset(route('leave_requests.create'))}}" class="btn btn-success  mt-2 float-right"
                   id="newUser">+
                    {{__('config.leave_requests.create')}}</a>
            @endcan
        </div>
        <br><br><br>
        @if(empty($leave_requests))
            <div class='panel-container'>
                <div class='panel-content' id='requests'>
                    <hr>
                    <div class="text-center">
                        <h1>{{__('config.leave_requests.no_data')}}</h1>
                    </div>
                </div>
            </div>
        @else
            <section class="section">
                <table class="display table" id="tableLeaveRequest">
                    <thead>
                    <tr class="text-center">
                        <th scope="col" width="10%">{{__('config.leave_requests.table.id')}}</th>
                        <th scope="col" width="20%">{{__('config.leave_requests.table.sender')}}</th>
                        <th scope="col" width="20%">{{__('config.leave_requests.table.content')}}</th>
                        <th scope="col" width="15%">{{__('config.leave_requests.table.start_at')}}</th>
                        <th scope="col" width="15%">{{__('config.leave_requests.table.end_at')}}</th>
                        <th scope="col" width="10%">{{__('config.leave_requests.table.status')}}</th>
                        <th scope="col" width="5%">{{__('config.leave_requests.table.view')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @can('isStaff')
                        @foreach($leave_requestPersons as $leave_requestPerson)
                            <tr>
                                <th scope="row">{{$leave_requestPerson->id}}</th>
                                <td>{{$leave_requestPerson->user->getFullNameAttribute()}}</td>
                                <td> {!! substr($leave_requestPerson->content, 0,20) . '  ...' !!}</td>
                                <td>{{$leave_requestPerson->start_at}}</td>
                                <td>{{$leave_requestPerson->end_at}}</td>
                                <td>
                                    @if($leave_requestPerson->status ==='pending')
                                        <span class="color-info">
                                        {{$leave_requestPerson->status}}
                                    </span>
                                    @elseif($leave_requestPerson->status ==='approved')
                                        <span class="color-success">
                                        {{$leave_requestPerson->status}}
                                    </span>
                                    @else
                                        <span class="color-warning">
                                        {{$leave_requestPerson->status}}
                                        </span>
                                    @endif
                                </td>
                                <td>
                                    @if($leave_requestPerson->status ==='pending')
                                        <a data-toggle="modal" href="#myModal{{$leave_requestPerson->id}}"
                                           title="view info"
                                           class="btn btn-info"><i
                                                class="fas fa-eye"></i></a>
                                    @elseif($leave_requestPerson->status ==='approved')
                                        <a data-toggle="modal" href="#myModal{{$leave_requestPerson->id}}"
                                           title="view info"
                                           class="btn btn-success"><i
                                                class="fas fa-eye"></i></a>
                                    @else
                                        <a data-toggle="modal" href="#myModal{{$leave_requestPerson->id}}"
                                           title="view info"
                                           class="btn btn-warning"><i
                                                class="fas fa-eye"></i></a>
                                    @endif
                                    <div class="modal" id="myModal{{$leave_requestPerson->id}}" data-keyboard="false"
                                         data-backdrop="static">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">{{__('config.leave_requests.modal.header')}}</h4>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-hidden="true">×
                                                    </button>
                                                </div>
                                                <div class="container"></div>
                                                <div class="modal-body">
                                                    <div>
                                                        <label for="recipient-name"
                                                               class="col-form-label font-bold color-dark">{{__('config.leave_requests.modal.fullname')}}</label>
                                                        <span
                                                            class="color-dark">{{$leave_requestPerson->user->getFullNameAttribute()}}</span>
                                                    </div>
                                                    <div>
                                                        <label for="recipient-name"
                                                               class="col-form-label font-bold color-dark">{{__('config.leave_requests.modal.position')}}</label>
                                                        <span class="color-dark">
                                                        @foreach($leave_requestPerson->user->positions as $position)
                                                                {{$position->name}},
                                                            @endforeach
                                                    </span>
                                                    </div>
                                                    <div>
                                                        <label for="recipient-name"
                                                               class="col-form-label font-bold color-dark">{{__('config.leave_requests.modal.division')}}</label>
                                                        <span
                                                            class="color-dark">{{$leave_requestPerson->user->division}}</span>
                                                    </div>
                                                    <div>
                                                        <label for="recipient-name"
                                                               class="col-form-label font-bold color-dark">{{__('config.leave_requests.modal.timeoff')}}</label>
                                                        <span
                                                            class="color-dark">{{$leave_requestPerson->getDayTwoDay()}} {{__('config.leave_requests.modal.days')}}</span>
                                                    </div>
                                                    <div>
                                                        <label for="recipient-name"
                                                               class="col-form-label font-bold color-dark">{{__('config.leave_requests.modal.from')}}</label>
                                                        <span
                                                            class="color-dark">{{$leave_requestPerson->start_at}} </span>
                                                        <label for="recipient-name"
                                                               class="col-form-label font-bold color-dark ml-4">{{__('config.leave_requests.modal.to')}}</label>
                                                        <span class="color-dark">{{$leave_requestPerson->end_at}}</span>
                                                    </div>
                                                    <div>
                                                        <label for="recipient-name"
                                                               class="col-form-label font-bold color-dark">{{__('config.leave_requests.modal.status')}} </label>

                                                        <span
                                                            class="color-dark">{{ $leave_requestPerson->status }}</span>
                                                    </div>
                                                    <div>
                                                        <label for="recipient-name"
                                                               class="col-form-label font-bold color-dark">{{__('config.leave_requests.modal.reason')}}</label>
                                                        <br>
                                                        <span
                                                            class="color-dark">{!! $leave_requestPerson->content !!}</span>
                                                    </div>
                                                    <div>
                                                        <label for="recipient-name"
                                                               class="col-form-label font-bold color-dark">{{__('config.leave_requests.modal.status')}} </label>

                                                        <span
                                                            class="color-dark">{{ $leave_requestPerson->status }}</span>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <a href="#" data-dismiss="modal"
                                                       class="btn">{{__('config.leave_requests.modal.close')}}</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        @foreach($leave_requests as $leave_request)
                            <tr>
                                <th scope="row">{{$leave_request->id}}</th>
                                <td>{{$leave_request->user->getFullNameAttribute()}}</td>
                                <td> {!! substr($leave_request->content, 0,20) . '  ...' !!}</td>
                                <td>{{$leave_request->start_at}}</td>
                                <td>{{$leave_request->end_at}}</td>
                                <td>
                                    @if($leave_request->status ==='pending')
                                        <span class="color-info">
                                        {{$leave_request->status}}
                                    </span>
                                    @elseif($leave_request->status ==='approved')
                                        <span class="color-success">
                                        {{$leave_request->status}}
                                    </span>
                                    @else
                                        <span class="color-warning">
                                        {{$leave_request->status}}
                                    </span>
                                    @endif
                                </td>
                                <td>
                                    @if($leave_request->status ==='pending')
                                        <a data-toggle="modal" href="#myModal{{$leave_request->id}}" title="view info"
                                           class="btn btn-info"><i
                                                class="fas fa-eye"></i></a>
                                    @elseif($leave_request->status ==='approved')
                                        <a data-toggle="modal" href="#myModal{{$leave_request->id}}" title="view info"
                                           class="btn btn-success"><i
                                                class="fas fa-eye"></i></a>
                                    @else
                                        <a data-toggle="modal" href="#myModal{{$leave_request->id}}" title="view info"
                                           class="btn btn-warning"><i
                                                class="fas fa-eye"></i></a>
                                    @endif
                                    <div class="modal" id="myModal{{$leave_request->id}}" data-keyboard="false"
                                         data-backdrop="static">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">{{__('config.leave_requests.modal.header')}}</h4>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-hidden="true">×
                                                    </button>
                                                </div>
                                                <div class="container"></div>
                                                <div class="modal-body">
                                                    <div>
                                                        <label for="recipient-name"
                                                               class="col-form-label font-bold color-dark">{{__('config.leave_requests.modal.fullname')}}</label>
                                                        <span
                                                            class="color-dark">{{$leave_request->user->getFullNameAttribute()}}</span>
                                                    </div>
                                                    <div>
                                                        <label for="recipient-name"
                                                               class="col-form-label font-bold color-dark">{{__('config.leave_requests.modal.position')}}</label>
                                                        <span class="color-dark">
                                                        @foreach($leave_request->user->positions as $position)
                                                                {{$position->name}},
                                                            @endforeach
                                                    </span>
                                                    </div>
                                                    <div>
                                                        <label for="recipient-name"
                                                               class="col-form-label font-bold color-dark">{{__('config.leave_requests.modal.division')}} </label>
                                                        <span
                                                            class="color-dark">{{$leave_request->user->division}}</span>
                                                    </div>
                                                    <div>
                                                        <label for="recipient-name"
                                                               class="col-form-label font-bold color-dark">{{__('config.leave_requests.modal.timeoff')}}</label>
                                                        <span
                                                            class="color-dark">{{$leave_request->getDayTwoDay()}} {{__('config.leave_requests.modal.days')}}</span>
                                                    </div>
                                                    <div>
                                                        <label for="recipient-name"
                                                               class="col-form-label font-bold color-dark">{{__('config.leave_requests.modal.from')}}</label>
                                                        <span class="color-dark">{{$leave_request->start_at}} </span>
                                                        <label for="recipient-name"
                                                               class="col-form-label font-bold color-dark ml-5">{{__('config.leave_requests.modal.to')}}</label>
                                                        <span class="color-dark">{{$leave_request->end_at}}</span>
                                                    </div>
                                                    <div>
                                                        <label for="recipient-name"
                                                               class="col-form-label font-bold color-dark">{{__('config.leave_requests.modal.status')}}</label>
                                                        <span class="color-dark">{{$leave_request->status}}</span>
                                                    </div>
                                                    <div>
                                                        <label for="recipient-name"
                                                               class="col-form-label font-bold color-dark">{{__('config.leave_requests.modal.reason')}}</label>
                                                        <br>
                                                        <span class="color-dark">{!! $leave_request->content !!}</span>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <a href="#" data-dismiss="modal"
                                                       class="btn">{{__('config.leave_requests.modal.close')}}</a>
                                                    @if($leave_request->status === 'pending')
                                                        <a href="{{route('leave_requests.accept',['id'=>$leave_request->id])}}"
                                                           class="btn btn-primary"
                                                           onclick="return confirm('Are you sure you want to accept?');">{{__('config.leave_requests.modal.accept')}}</a>
                                                        <a data-toggle="modal" href="#myModal2{{$leave_request->id}}"
                                                           class="btn btn-dark">{{__('config.leave_requests.modal.deny')}}</a>
                                                        <div class="modal" id="myModal2{{$leave_request->id}}" data-backdrop="static">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h4 class="modal-title">{{__('config.leave_requests.modal.message_deny')}}</h4>
                                                                        <button type="button" class="close" data-dismiss="modal"
                                                                                aria-hidden="true">×
                                                                        </button>
                                                                    </div>
                                                                    <div class="container"></div>
                                                                    <form
                                                                        action="{{route('leave_requests.deny',['id' => $leave_request->id])}}"
                                                                        method="post">
                                                                        @csrf
                                                                        <div class="modal-body">
                                                                            <div class="form-group">
                                                                                <label
                                                                                    class="col-form-label">{{__('config.leave_requests.modal.received')}}</label>
                                                                                <input type="text" class="form-control"
                                                                                       value="{{$leave_request->user->getFullNameAttribute()}}"
                                                                                       readonly>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="message-text{{$leave_request->id}}"
                                                                                       class="col-form-label">{{__('config.leave_requests.modal.reason')}}</label>
                                                                                <textarea class="form-control" rows="8" name="content_deny"
                                                                                          id="message-text{{$leave_request->id}}"></textarea>
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <a href="#" data-dismiss="modal"
                                                                               class="btn">{{__('config.leave_requests.modal.close')}}</a>
                                                                            <button type="submit"
                                                                                    class="btn btn-primary">{{__('config.leave_requests.modal.message_deny')}}</button>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </section>
        @endif
    </div>
@endsection
