@section('css_timesheet_management')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.css" integrity="sha512-bYPO5jmStZ9WI2602V2zaivdAnbAhtfzmxnEGh9RwtlI00I9s8ulGe4oBa5XxiC6tCITJH/QG70jswBhbLkxPw==" crossorigin="anonymous" />
@endsection
@section('title_timesheet_management')
{{__('config.leave_requests_add.title')}}
@endsection
@section('active_listUser','active')
@section('js_timesheet_management')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.full.min.js" integrity="sha512-AIOTidJAcHBH2G/oZv9viEGXRqDNmfdPVPYOYKGy3fti0xIplnlgMHUGfuNRzC6FkzIo0iIxgFnr9RikFxK+sw==" crossorigin="anonymous"></script>
    <script src="https://cdn.ckeditor.com/4.15.1/standard/ckeditor.js"></script>
    <script type="text/javascript">
        $(function () {
            $('#start_at').datetimepicker({
                dateFormat: 'dd/mm/yy',
                timeFormat: 'hh:mm',
                minDate: new Date(),
                value: new Date()
            });
            $('#end_at').datetimepicker({
                dateFormat: 'dd/mm/yy',
                timeFormat: 'hh:mm',
                minDate: new Date(),
                value: new Date()
            });
        });
        CKEDITOR.replace( 'content' );
    </script>
@endsection
@extends('layouts.timesheet_management')
@section('content_timesheet_management')
    <div class="main-content container-fluid">
        <div class="page-title ">
            <h3 class="float-left">{{__('config.leave_requests_add.header')}}</h3>
        </div>
        <br><br><br>
        <section class="section">
            <form id="addUser" class="col-12" action="{{route('leave_requests.store')}}" method="post">
                @csrf
                <div class="row">
                    <div class="col-4">
                        <label for="start_at">{{__('config.leave_requests_add.start_at')}}</label>
                        <input type="text" id="start_at" class="form-control @error('start_at') is-invalid @enderror datetimepicker-input" data-toggle="datetimepicker" data-target="#start_at"
                               name="start_at"
                               value="{{old('start_at')}}">
                        @error('start_at')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="col-4">
                        <label for="end_at">{{__('config.leave_requests_add.end_at')}}</label>
                        <input type="text" id="end_at" class="form-control @error('end_at') is-invalid @enderror datetimepicker-input" data-toggle="datetimepicker" data-target="#end_at"
                               name="end_at"
                               value="{{old('end_at')}}">
                        @error('end_at')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-4">
                        <label for="phone">{{__('config.leave_requests_add.phone')}}</label>
                        <input type="tel" class="form-control @error('phone') is-invalid @enderror" id="phone"
                               name="phone" pattern="^\+?(?:[0-9]??).{9,12}[0-9]$" required
                               value="{{$user->phone??""}}">
                        @error('phone')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-8">
                        <label for="content">{{__('config.leave_requests_add.content')}}</label>
                        <textarea class="form-control @error('content') is-invalid @enderror" aria-label="With textarea" id="content" name="content"></textarea>
                        @error('content')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <button type="submit" class="btn btn-primary mt-3">{{__('config.leave_requests_add.submit')}}</button>
            </form>
        </section>
    </div>
@endsection

