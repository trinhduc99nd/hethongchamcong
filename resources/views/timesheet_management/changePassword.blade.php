@section('css_timesheet_management')
@endsection
@section('title_timesheet_management')
{{__('config.change_password.title')}}
@endsection
@extends('layouts.timesheet_management')
@section('content_timesheet_management')
    <div class="main-content container-fluid">
        <div class="page-title ">
            <h3 class="float-left">{{__('config.change_password.header')}}</h3>
        </div>
        <br><br><br>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">{{__('config.change_password.change_pass')}}
                        </div>
                        <div class="card-body">
                            <form method="POST" action="{{ route('change.password') }}">
                                @csrf
                                @foreach ($errors->all() as $error)
                                    <p class="text-danger">{{ $error }}</p>
                                @endforeach
                                <div class="form-group row">
                                    <label for="password" class="col-md-4 col-form-label text-md-right">{{__('config.change_password.current_pass')}}</label>
                                    <div class="col-md-6">
                                        <input id="password" type="password" class="form-control"
                                               name="current_password" autocomplete="current-password">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="new_password" class="col-md-4 col-form-label text-md-right">{{__('config.change_password.new_pass')}}</label>
                                    <div class="col-md-6">
                                        <input id="new_password" type="password" class="form-control"
                                               name="new_password" autocomplete="current-password">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="new_confirm_password" class="col-md-4 col-form-label text-md-right">{{__('config.change_password.new_confirm_pass')}}</label>
                                    <div class="col-md-6">
                                        <input id="new_confirm_password" type="password" class="form-control"
                                               name="new_confirm_password" autocomplete="current-password">
                                    </div>
                                </div>
                                <div class="form-group row mb-0">
                                    <div class="col-md-8 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            {{__('config.change_password.update')}}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

