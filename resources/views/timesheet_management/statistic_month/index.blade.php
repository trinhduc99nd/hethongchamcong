@section('css_timesheet_management')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.dataTables.min.css">
    <style>
        .error {
            color: red;
        }

        button.dt-button.buttons-pdf.buttons-html5 {
            background: #baba1e;
        }

        button.dt-button.buttons-excel.buttons-html5 {
            background: #23dc23;
        }

        button.dt-button.buttons-print {
            background: #423c3c;
        }
    </style>
@endsection
@section('title_timesheet_management')
    {{__('config.statistic.title')}}
@endsection
@section('active_static','active')
@section('js_timesheet_management')
    <script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.min.js"></script>
    <script src="{{asset('js/static.js')}}"></script>
@endsection
@extends('layouts.timesheet_management')
@section('content_timesheet_management')
    <div class="main-content container-fluid">
        <div class="page-title ">
            <h3 class="float-left">{{__('config.statistic.header')}}</h3>
        </div>
        <br><br><br>
        <form id="post-form" class="mb-3" method="post" action="javascript:void(0)">
            <div class="col-9">
                <div class="row">
                    <div class="col">
                        <label for="year">{{__('config.statistic.select_year')}}</label>
                        <select class="form-control" id="year" name="year">
                            <option value="">{{__('config.statistic.select_year')}}</option>
                            @foreach($years  as $year)
                                <option value="{{$year->year}}">{{$year->year}}</option>
                            @endforeach
                        </select>
                        <span class="text-danger">{{ $errors->first('year') }}</span>
                    </div>
                    <div class="col">
                        <label for="month">{{__('config.statistic.select_month')}}</label>
                        <select class="form-control" id="month" name="month">
                            <option value="">{{__('config.statistic.select_month')}}</option>
                        </select>
                        <span class="text-danger">{{ $errors->first('month') }}</span>
                    </div>
                    <div class="col">
                        <button type="submit" id="send_form" class="btn btn-block btn-primary"
                                style="margin-top: 21px; width: 100%">
                            {{__('config.statistic.submit')}}
                        </button>
                    </div>
                </div>
            </div>
        </form>
        <section class="section mt-5">
            <table class="table display nowrap  table-bordered table-striped table-hover" id="tableLeaveRequest">
                <thead>
                <tr class="text-center">
                    <th scope="col" width="18%">{{__('config.statistic.table.username')}}</th>
                    <th scope="col" width="26%">{{__('config.statistic.table.total_days_in_month')}}</th>
                    <th scope="col" width="26%">{{__('config.statistic.table.total_hours_in_month')}}</th>
                    <th scope="col" width="25%">{{__('config.statistic.table.total_leave_days_in_month')}}</th>
                    <th scope="col" width="5%">View</th>
                </tr>
                </thead>
                <tbody id="table_data">
                </tbody>
            </table>
        </section>
    </div>
@endsection

