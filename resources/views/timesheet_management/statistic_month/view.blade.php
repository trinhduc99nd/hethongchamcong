@section('css_timesheet_management')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.dataTables.min.css">
    <style>
        .error {
            color: red;
        }

        button.dt-button.buttons-pdf.buttons-html5 {
            background: #baba1e;
        }

        button.dt-button.buttons-excel.buttons-html5 {
            background: #23dc23;
        }

        button.dt-button.buttons-print {
            background: #423c3c;
        }
    </style>
@endsection
@section('title_timesheet_management')
    {{__('config.statistic.view.title')}} {{$month}}-{{$year}} của {{$data->users->getFullNameAttribute()}}

@endsection
@section('active_static','active')
@section('js_timesheet_management')
    <script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.min.js"></script>
    <script>
        $('#tableLeaveRequest').DataTable({
            "order": [[0, "desc"]],
            retrieve: true,
            paging: false,
            dom: 'Bfrtip',
            buttons: [
                'csv', 'excel', 'pdf', 'print'
            ],
        });
    </script>
@endsection
@extends('layouts.timesheet_management')
@section('content_timesheet_management')
    <div class="main-content container-fluid">
        <div class="page-title ">
            <h3 class="float-left">{{__('config.statistic.view.title')}} {{$month}}-{{$year}}
                của {{$data->users->getFullNameAttribute()}}</h3>
        </div>
        <section class="section mt-5">
            <div class="header-content mt-5">
                <div class="row">
                    <div class="col-md-6">
                        <table>
                            <tr>
                                <th>
                                    {{__('config.statistic.view.user_name')}}
                                </th>
                                <td>
                                    :{{$data->users->getFullNameAttribute()}}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    {{__('config.statistic.view.email')}}
                                </th>
                                <td>
                                    :{{$data->users->email}}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    {{__('config.statistic.view.position')}}
                                </th>
                                <td>:
                                    @for($i = 0; $i < count($data->users->positions); $i++)
                                        @if($i == count($data->users->positions) - 1)
                                            {{$data->users->positions[$i]->name}}
                                        @else
                                            {{$data->users->positions[$i]->name}},
                                        @endif
                                    @endfor
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    {{__('config.statistic.view.division')}}
                                </th>
                                <td>
                                    : {{$data->users->division}}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    {{__('config.statistic.view.total_day')}}
                                </th>
                                <td>
                                    :{{$data->working_days_count}} <b>Days</b>
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    {{__('config.statistic.view.total_hour')}}
                                </th>
                                <td>
                                    :{{$data->working_hours_count}} <b>Hours</b>
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    {{__('config.statistic.view.total_leave_off')}}
                                </th>
                                <td>
                                    : {{$data->leave_days_count??'Không có'}} <b>Days</b>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="table-content mt-5">
                <table class="table display nowrap  table-bordered table-striped table-hover" id="tableLeaveRequest">
                    <thead>
                    <tr>
                        <th scope="col" width="25%">{{__('config.statistic.view.table.day')}}</th>
                        <th scope="col" width="25%">{{__('config.statistic.view.table.check_in')}}</th>
                        <th scope="col" width="25%">{{__('config.statistic.view.table.check_out')}}</th>
                        <th scope="col" width="25%">{{__('config.statistic.view.table.total_hour_working')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($timesheetInMonths as $timesheetInMonths)
                        <tr>
                            <td>{{$timesheetInMonths->working_on_day}}</td>
                            <td>{{$timesheetInMonths->check_in}}</td>
                            <td>{{$timesheetInMonths->check_out}}</td>
                            <td>{{(string)gmdate('H:i:s', $timesheetInMonths->countHourInDay())}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </section>
    </div>
@endsection

