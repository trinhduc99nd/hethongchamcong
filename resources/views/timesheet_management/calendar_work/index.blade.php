@section('css_timesheet_management')
    <link rel='stylesheet' href='{{asset('https://cdn.jsdelivr.net/npm/fullcalendar@5.4.0/main.min.css')}}'/>
@endsection
@section('title_timesheet_management')
    {{__('config.calendar_work.title')}}
@endsection
@section('active_home', 'active')
@extends('layouts.timesheet_management')
@section('content_timesheet_management')
    <div class="main-content container-fluid">
        <div class="page-title ">
            <h3 class="float-left">{{__('config.calendar_work.calendar')}}</h3>
            <a class="btn btn-primary ml-2 float-right" id="checkout">{{__('config.calendar_work.checkout')}}</a>
            <form id="checkout-form" action="{{ route('user_check.checkout') }}" method="POST"
                  style="display: none;">
                @csrf
            </form>
            <a class="btn btn-light float-right" id="checkin">{{__('config.calendar_work.checkin')}}</a>
            <form id="checkin-form" action="{{ route('user_check.checkin') }}" method="POST"
                  style="display: none;">
                @csrf
            </form>
        </div>
        <br>
        <br><br>
        <section class="section">
            <div id='calendar'></div>
        </section>
    </div>
@endsection
@section('js_timesheet_management')
    <script src='{{asset('https://cdn.jsdelivr.net/npm/fullcalendar@5.4.0/main.min.js')}}'></script>
    <script>
        document.addEventListener('DOMContentLoaded', function () {
            const calendarEl = document.getElementById('calendar');
            const events =  {!! $events !!}
            const calendar = new FullCalendar.Calendar(calendarEl, {
                    initialView: 'dayGridMonth',
                googleCalendarApiKey: 'AIzaSyDcnW6WejpTOCffshGDDb4neIrXVUA1EAE',
                headerToolbar: {
                        left: 'prev,next today',
                        center: 'title',
                        right: 'dayGridMonth,dayGridWeek,listMonth'
                    },
                    eventSources: [
                        {
                            googleCalendarId: 'vi.vietnamese#holiday@group.v.calendar.google.com',
                            color: 'yellow',   // an option!
                            textColor: 'black'
                        },
                        {
                            events,
                        },
                    ],
                    eventTimeFormat: { // like '14:30:00'
                        hour: '2-digit',
                        minute: '2-digit',
                        second: '2-digit',
                        hour12: false
                    },
                })
            ;
            calendar.render();
        });
        $(document).ready(function () {
            $('#checkin').click(function () {
                if (confirm('{{__('config.calendar_work.confirm_checkin')}}')) {
                    $('#checkin-form').submit();
                }
                return false;
            });
            $('#checkout').click(function () {
                if (confirm('{{__('config.calendar_work.confirm_checkout')}}')) {
                    $('#checkout-form').submit();
                }
                return false;
            });
        });
    </script>
@endsection
