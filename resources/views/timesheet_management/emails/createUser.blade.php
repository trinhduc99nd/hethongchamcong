<!DOCTYPE html>
<html>
<head>
    <title>{{__('config.email.user.title')}}</title>
</head>
<body>
<h1>{{__('config.email.user.h1')}}</h1>
<h2>{{__('config.email.user.h2')}}</h2>
<h4> {{__('config.email.user.email')}}{{ $details['email'] }}</h4>
<h4>{{__('config.email.user.password')}}{{ $details['password'] }}</h4>
<p>{{__('config.email.user.content')}} <a href="{{url('/login')}}">{{__('config.email.user.title')}}</a> </p>
<p>{{__('config.email.user.thank_you')}}</p>
</body>
</html>
