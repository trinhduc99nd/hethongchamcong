<!DOCTYPE html>
<html>
<head>
    <title>{{__('config.email.leave_request_admin.title')}}</title>
    <link rel="stylesheet" href="{{asset('admin/assets/css/bootstrap.css')}}">
</head>
<body>
<h1>{{$data['title']}}</h1>
<h3>{{__('config.email.leave_request_admin.my_name_is')}} {{$data['name']}}</h3>
<h4>{{__('config.email.leave_request_admin.reason')}} {!! $data['content'] !!}</h4>
<p>{{__('config.email.leave_request_admin.thank_you')}}</p>
</body>
</html>
