<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title_timesheet_management')</title>
    <base href="{{asset('')}}">
    <link rel="stylesheet" href="{{asset('admin/assets/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('admin/assets/vendors/chartjs/Chart.min.css')}}">
    <link rel="stylesheet" href="{{asset('admin/assets/vendors/perfect-scrollbar/perfect-scrollbar.css')}}">
    <link rel="stylesheet" href="{{asset('admin/assets/css/app.css')}}">
    <link rel="stylesheet" href="{{asset('admin/assets/css/main.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
    <link rel="shortcut icon" href="{{asset('admin/assets/images/favicon.svg')}}" type="image/x-icon">
    @yield('css_timesheet_management')
    @toastr_css
</head>
<body>
<div id="app">
    @include('partial.sidebar')
    <div id="main">
        @include('partial.header')
        @yield('content_timesheet_management')
        @include('partial.footer')
    </div>
</div>
<script src="{{asset('vendor/sweetalert/sweetalert.all.js')}}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
@include('sweetalert::alert')
@toastr_js
@toastr_render
<script src="{{asset('admin/assets/js/feather-icons/feather.min.js')}}"></script>
<script src="{{asset('admin/assets/vendors/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
<script src="{{asset('admin/assets/js/app.js')}}"></script>
<script src="{{asset('admin/assets/vendors/chartjs/Chart.min.js')}}"></script>
<script src="{{asset('admin/assets/vendors/apexcharts/apexcharts.min.js')}}"></script>
<script src="{{asset('admin/assets/js/main.js')}}"></script>
<script>
    $(function () {
        $('#logout').click(function () {
            if (confirm('{{__('config.confirm_logout')}}')) {
                document.getElementById('logout-form').submit();
            }
            return false;
        });
    });
    notificationData();

    function timeSince(date) {
        let seconds = Math.floor((new Date() - date) / 1000);
        let interval = seconds / 31536000;
        if (interval > 1) {
            return Math.floor(interval) + " years before";
        }
        interval = seconds / 2592000;
        if (interval > 1) {
            return Math.floor(interval) + " months before";
        }
        interval = seconds / 86400;
        if (interval > 1) {
            return Math.floor(interval) + " days before";
        }
        interval = seconds / 3600;
        if (interval > 1) {
            return Math.floor(interval) + " hours before";
        }
        interval = seconds / 60;
        if (interval > 1) {
            return Math.floor(interval) + " minutes before";
        }
        return Math.floor(seconds) + " seconds before";
    }

    function notificationData() {
        const $url = '{{route('notify.send')}}';
        $.ajax({
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: $url,
            success: function (res) {
                if (res) {
                    $('#notification').empty();
                    $.each(res.reverse(), function (key, value) {
                        let html;
                        if (value.data['day']) {
                            html = `<li class="list-group-item border-0 align-items-start">
                                <div>
                                    <h6 class='text-bold '>` + value.data['title'] + `</h6>
                                    <h6>Người gửi : ` + value.data['name'] + `</h6>
                                    <p class='text-xs'>
                                      Day Forward :` + value.data['day'] + `
                                    </p>
                                    <span>` + timeSince(new Date(value.created_at)) + `</span>
                                    <p class='text-xs'>
                                      Content:` + value.data['content'] + `
                                    </p>
                                </div>
                            </li> <hr>`
                        } else if (value.data['reason']) {
                            html = `<li class="list-group-item border-0 align-items-start">
                                <div>
                                    <h6 class='text-bold'>` + value.data['title'] + `</h6>
                                    <span>` + timeSince(new Date(value.created_at)) + `</span>
                                    <p class='text-xs'>
                                      Content:` + value.data['content'] + `
                                    </p>
                                    <p class='text-xs'>
                                      Reason:` + value.data['reason'] + `
                                    </p>
                                </div>
                            </li> <hr>`
                        } else if (value.data['name']) {
                            html = `<li class="list-group-item border-0 align-items-start">
                                <div>
                                    <h6 class='text-bold'>` + value.data['title'] + `</h6>
                                    <h6>Người gửi : ` + value.data['name'] + `</h6>
                                    <span>` + timeSince(new Date(value.created_at)) + `</span>
                                    <p class='text-xs'>
                                      Content:` + value.data['content'] + `
                                    </p>
                                </div>
                            </li> <hr>`
                        } else {
                            html = `
                            <li class="list-group-item border-0 align-items-start">
                                <div>
                                    <h6 class='text-bold'>` + value.data['title'] + `</h6>
                                    <span>` + timeSince(new Date(value.created_at)) + `</span>
                                    <p class='text-xs'>
                                      Content:` + value.data['content'] + `
                                    </p>
                                </div>
                            </li> <hr>`
                        }
                        $('#notification').prepend(html);
                    });
                }
            }
        });
    }
</script>
@yield('js_timesheet_management')
</body>
</html>
