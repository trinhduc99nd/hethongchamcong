<footer>
    <div class="footer clearfix mb-0 text-muted mt-5">
        <div class="float-left">
            <p>2020 &copy;</p>
        </div>
        <div class="float-right">
            <p>Duc<span class='text-danger'><i data-feather="heart"></i></span> by <a href="http://ahmadsaugi.com"></a></p>
        </div>
    </div>
</footer>
