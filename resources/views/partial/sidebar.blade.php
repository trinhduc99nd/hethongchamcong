<div id="sidebar" class='active'>
    <div class="sidebar-wrapper active">
        <div class="sidebar-header">
            <img src="{{asset('admin/assets/images/logo.png')}}" alt="" srcset="">
        </div>
        <div class="sidebar-menu">
            <ul class="menu">
                <li class="sidebar-item @yield('active_home')">
                    <a href="{{route('user_check.index')}}" class='sidebar-link'>
                        <i data-feather="home" width="20"></i>
                        <span>{{__('sidebar.calendar')}}</span>
                    </a>
                </li>
                @can('isAdmin')
                    <li class="sidebar-item  has-sub @yield('active_history')">
                        <a href="{{route('person.personList')}}" class='sidebar-link'>
                            <i data-feather="triangle" width="20"></i>
                            <span>{{__('sidebar.historyTimeSheet')}}</span>
                        </a>
                        <ul class="submenu">
                            <li>
                                <a href="{{route('person.personList')}}">{{__('sidebar.person')}}</a>
                            </li>
                            <li>
                                <a href="{{route('people.peopleList')}}">{{__('sidebar.people')}}</a>
                            </li>
                        </ul>
                    </li>
                @endcan
                @can('isStaff')
                    <li class="sidebar-item @yield('active_history')">
                        <a href="{{route('person.personList')}}" class='sidebar-link'>
                            <i data-feather="triangle" width="20"></i>
                            <span>{{__('sidebar.historyTimeSheet')}}</span>
                        </a>
                    </li>
                @endcan
                @can('isAdmin')
                    <li class="sidebar-item @yield('active_leaveRequest')">
                        <a href="{{route('leave_requests.index')}}" class='sidebar-link'>
                            <i data-feather="layers" width="20"></i>
                            <span>{{__('sidebar.list_request')}}</span>
                        </a>
                    </li>
                @endcan
                @can('isStaff')
                    <li class="sidebar-item @yield('active_leaveRequest')">
                        <a href="{{route('leave_requests.index')}}" class='sidebar-link'>
                            <i data-feather="layers" width="20"></i>
                            <span>{{__('sidebar.list_request_off')}}</span>
                        </a>
                    </li>
                @endcan
                <li class="sidebar-item  @yield('active_static')">
                    <a href="{{route('statistic.index')}}" class='sidebar-link'>
                        <i data-feather="grid" width="20"></i>
                        <span>{{__('sidebar.statistic')}}</span>
                    </a>
                </li>
                @can('isAdmin')
                    <li class="sidebar-item  @yield('active_listUser')">
                        <a href="{{route('users.index')}}" class='sidebar-link'>
                            <i data-feather="file-plus" width="20"></i>
                            <span>{{__('sidebar.list_user')}}</span>
                        </a>
                    </li>
                @endcan
            </ul>
        </div>
        <button class="sidebar-toggler btn x"><i data-feather="x"></i></button>
    </div>
</div>
