
<nav class="navbar navbar-header navbar-expand navbar-light">
    <a class="sidebar-toggler" href="#"><span class="navbar-toggler-icon"></span></a>
    <button class="btn navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav d-flex align-items-center navbar-light ml-auto">
            <li class="dropdown nav-icon">
                <a href="#" data-toggle="dropdown" class="nav-link  dropdown-toggle nav-link-lg nav-link-user">
                    <div class="d-lg-inline-block">
                        <i data-feather="bell"></i>
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-right dropdown-menu-large">
                    <h5 class='py-2 px-4 font-bold'>{{__('header.notification')}}</h5>
                    <ul class="list-group rounded-none" id="notification">
                    </ul>
                </div>
            </li>
            <li class="dropdown">
                <a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
                    <div class="avatar mr-1">
                        <img src="{{asset(\Illuminate\Support\Facades\Auth::user()->profile_image??'admin/assets/images/avatar.png')}}" alt="" srcset="">
                    </div>
                    <div class="d-none d-md-block d-lg-inline-block">{{ __('header.welcome',['Name' => \Illuminate\Support\Facades\Auth::user()->getFullNameAttribute()])}}</div>
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="{{route('users.info')}}"><i data-feather="user"></i>{{ __('header.info')}}</a>
                    <a class="dropdown-item" href="{{url('change-password')}}"><i data-feather="edit-2"></i>{{ __('header.change_password')}}</a>
                    <div class="dropdown-divider">
                    </div>
                    <a class="dropdown-item" href="#" id="logout"><i data-feather="log-out"></i>{{__('header.logout')}}</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                          style="display: none;">
                        @csrf
                    </form>
                </div>
            </li>
            @php $locale = Session::get('language');
            @endphp
            <li class="nav-item dropdown">
                <a id="navbarDropdown" class="dropdown-toggle" href="#" role="button"
                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    @switch($locale)
                        @case('en')
                        <img src="{{asset('img/en.png')}}" alt="" width="40px" height="25px"> English
                        @break
                        @case('vi')
                        <img src="{{asset('img/vn.png')}}" alt="" width="40px" height="25px"> Vietnamese
                        @break
                        @default
                        <img src="{{asset('img/en.png')}}" alt="" width="40px" height="25px"> English
                    @endswitch
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item bg-gray" href="{!! route('change-language', ['en']) !!}"><img src="{{asset('img/en.png')}}" alt="" width="40px" height="25px"> English</a>
                    <a class="dropdown-item bg-gray" href="{!! route('change-language', ['vi']) !!}"><img src="{{asset('img/vn.png')}}" alt="" width="40px" height="25px">VietNamese</a>
                </div>
            </li>
        </ul>
    </div>
</nav>
