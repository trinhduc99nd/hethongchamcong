$(document).ready(function () {
    const baseUrl = $('base').attr('href')
    $('#year').change(function () {
        let year = $("#year option:selected").val();
        if (year) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "post",
                url: "statistic/getMonth/" + year,
            }).done(function (res) {
                if (res) {
                    $("#month").empty().append('<option value="">Chọn Tháng</option>');
                    $.each(res, function (key, value) {
                        $("#month").append('<option value="' + value.month + '">' + value.month + '</option>');
                    });
                }
            })
        }
    });

    $("#post-form").validate({
        rules: {
            year: {
                required: true,
            }
            ,
            month: {
                required: true,
            }
        },
        messages: {
            month: {
                required: "Please choose month",
            },
            year: {
                required: "Please choose year",
            },
        },
        submitHandler: function () {
            let year = $("#year option:selected").val();
            let month = $("#month option:selected").val();

            function capitalizeFirstLetter(string) {
                return string.charAt(0).toUpperCase() + string.slice(1);
            }

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#send_form').html('Sending...');
            $.ajax({
                url: 'statistic/getData/' + year + '/' + month,
                type: "POST",
                success: function (res) {
                    $('#send_form').html('Submit');
                    if (res) {
                        $('#table_data').empty();
                        $.each(res, function (key, value) {
                            let html = '<tr class="text-center">';
                            html += '<td>' + capitalizeFirstLetter(value.users.last_name) + " " + capitalizeFirstLetter(value.users.first_name) + '</td>';
                            html += '<td>' + value.working_days_count + '</td>';
                            html += '<td>' + value.working_hours_count + '</td>';
                            html += '<td>' + value.leave_days_count + '</td>';
                            html += '<td>' + ' <a href="' + baseUrl + 'statistic/view/' + value.users.id + '/' + year + '/' + month + '"  title="delete user"><i class="fas fa-eye"></i></a>' + '</td></tr>';
                            $('#table_data').prepend(html);
                        });
                        $('#tableLeaveRequest').DataTable({
                            "order": [[0, "desc"]],
                            retrieve: true,
                            paging: false,
                            dom: 'Bfrtip',
                            buttons: [
                                'csv', 'excel', 'pdf', 'print'
                            ]
                        });
                    }
                }
            });
        }
    })
});
