$().ready(function() {

    $("#addUser").validate({
        onfocusout: false,
        onkeyup: false,
        onclick: false,
        rules: {
            first_name: {
                required: true,
            },
            last_name: {
                required: true,
            },
            email: {
                required: true,
                email: true,
            },
            role:{
                required:true,
            },
            division:{
                required:true,
            },
            position:{
                required:true,
            },
            password: {
                required: true,
                minlength: 6
            },
            confirm_password: {
                equalTo: "#password",
                minlength: 6
            }
        },
        messages:{
            first_name:{
                required:"Please enter your firstname"
            },
            last_name: {
                required: "Please enter your lastname"
            },
            email:{
                required:"Please enter your email"
            },
            password:{
                required:"Please provide a password",
                minlength:"Your password must be at least 6 characters long"
            }
        },
        submitHandler:
        function (form) {
            form.submit();
        }
    });
});
