<?php

use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PositionUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('positions_users')->delete();
        $positionsUsers = [
            ['position_id' => 1, 'user_id' => 1,],
            ['position_id' => 2, 'user_id' => 1,],
            ['position_id' => 1, 'user_id' => 2,],
            ['position_id' => 2, 'user_id' => 2,],
            ['position_id' => 4, 'user_id' => 2,],
            ['position_id' => 5, 'user_id' => 3,],
            ['position_id' => 2, 'user_id' => 3,],
            ['position_id' => 1, 'user_id' => 3,],
            ['position_id' => 5, 'user_id' => 4,],
            ['position_id' => 4, 'user_id' => 4,],
            ['position_id' => 6, 'user_id' => 4,],
        ];

        foreach ($positionsUsers as $user) {
            DB::table('positions_users')->insert($user);
        }


    }
}
