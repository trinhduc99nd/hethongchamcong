<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        $users = [
            [
                'id' => 1,
                'first_name' => 'Admin',
                'last_name' => 'Admin',
                'email' => 'admin@admin.com',
                'gender' => 'male',
                'role' => 'admin',
                'division' => '1',
                'password' => Hash::make('password'),
                'phone' => '0988746131',
            ],
            [
                'id' => 2,
                'first_name' => 'User 1',
                'last_name' => 'User',
                'email' => 'user1@gmail.com',
                'gender' => 'male',
                'role' => 'staff',
                'division' => '2',
                'password' => Hash::make('password'),
                'phone' => '0988746131',
            ],
            [
                'id' => 3,
                'first_name' => 'User 2',
                'last_name' => 'User',
                'email' => 'user2@gmail.com',
                'gender' => 'male',
                'role' => 'staff',
                'division' => '1',
                'password' => Hash::make('password'),
                'phone' => '0988746131',
            ],
            [
                'id' => 4,
                'first_name' => 'User 3',
                'last_name' => 'User',
                'email' => 'user3@gmail.com',
                'gender' => 'male',
                'role' => 'staff',
                'division' => '2',
                'password' => Hash::make('password'),
                'phone' => '0988746131',
            ]
        ];
        foreach ($users as $user) {
            \App\User::create($user);
        }
    }

}
