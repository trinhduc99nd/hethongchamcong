<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PositionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('positions')->delete();
        $users = [
            ['id' => 1, 'name' => 'Giám đốc nhân sự',],
            ['id' => 2, 'name' => 'Bảo vệ'],
            ['id' => 3, 'name' => 'Thực tập sinh'],
            ['id' => 4, 'name' => 'Developer'],
            ['id' => 5, 'name' => 'Hr'],
            ['id' => 6, 'name' => 'Kế toán'],
        ];

        foreach ($users as $user) {
            \App\Position::create($user);
        }
    }
}
