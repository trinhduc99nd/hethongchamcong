<?php

use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserCheckSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_checks')->delete();
        $faker = Factory::create();
        $limit = 100;
        for ($i = 0; $i < $limit; $i++) {
            $date = Carbon::create(2020, 9, 1);
            DB::table('user_checks')->insert([
                'working_on_day' =>   $date->addDays($i)->toDateString(),
                'check_in' => $faker->randomElement($array = array ('8:00:10','8:30:00','9:00:00')),
                'check_out' => $faker->randomElement($array = array ('17:15:10','17:30:00','18:00:00')),
                'user_id' => $faker->randomElement($array = array ('1','2','3','4')),
            ]);
        }
    }
}
