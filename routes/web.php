<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('change-language/{language}', 'HomeController@changeLanguage')->name('change-language');

Auth::routes(['register' => false, 'verify' => false]);

Route::group(['middleware' => 'localization'], function () {
    Route::prefix('')->group(function () {
        Route::get('/', [
            'as' => 'user_check.index',
            'uses' => 'UserCheckController@index',
        ]);
        Route::post('/checkin', [
            'as' => 'user_check.checkin',
            'uses' => 'UserCheckController@checkin',
        ]);
        Route::post('/checkout', [
            'as' => 'user_check.checkout',
            'uses' => 'UserCheckController@checkout',
        ]);
        Route::post('/notify', [
            'as' => 'notify.send',
            'uses' => 'NotificationController@index',
        ]);
        Route::get('/person', [
            'as' => 'person.personList',
            'uses' => 'ListCheckController@personList',
        ]);
        Route::get('/addProblem', [
            'as' => 'person.addProblem',
            'uses' => 'ListCheckController@addProblem',
        ]);
        Route::post('/storeProblem', [
            'as' => 'person.storeProblem',
            'uses' => 'ListCheckController@storeProblem',
        ]);
        Route::get('/people', [
            'as' => 'people.peopleList',
            'uses' => 'ListCheckController@peopleList',
        ]);
        Route::get('/updateCheckOut/{id}', [
            'as' => 'people.updateCheckOut',
            'uses' => 'ListCheckController@updateCheckOut',
        ]);
        Route::get('/updateCheckOut/{id}', [
            'as' => 'people.updateCheckOut',
            'uses' => 'ListCheckController@updateCheckOut',
        ]);
        Route::get('/viewCheck', [
            'as' => 'people.viewCheck',
            'uses' => 'ListCheckController@viewCheck',
        ]);
        Route::post('/storeCheck', [
            'as' => 'people.storeCheck',
            'uses' => 'ListCheckController@storeCheck',
        ]);
    });

    Route::prefix('/statistic')->group(function () {
        Route::get('/', [
            'as' => 'statistic.index',
            'uses' => 'StatisticController@index',
        ]);
        Route::get('/view/{id}/{year}/{month}', [
            'as' => 'statistic.view',
            'uses' => 'StatisticController@view',
        ]);
        Route::post('getMonth/{year}', [
            'as' => 'statistic.getMonth',
            'uses' => 'StatisticController@getMonth',
        ]);
        Route::post('getData/{year}/{month}', [
            'as' => 'statistic.getData',
            'uses' => 'StatisticController@getData',
        ]);
    });

    Route::prefix('/users')->group(function () {
        Route::get('/', [
            'as' => 'users.index',
            'uses' => 'UserController@index',
            'middleware' => 'can:user-view'
        ]);
        Route::get('/add', [
            'as' => 'users.add',
            'uses' => 'UserController@add',
            'middleware' => 'can:user-create'
        ]);
        Route::post('/create', [
            'as' => 'users.create',
            'uses' => 'UserController@create',
            'middleware' => 'can:user-create'
        ]);
        Route::get('/edit/{id}', [
            'as' => 'users.edit',
            'uses' => 'UserController@edit',
            'middleware' => 'can:user-update'
        ]);
        Route::post('/update/{id}', [
            'as' => 'users.update',
            'uses' => 'UserController@update',
            'middleware' => 'can:user-update'
        ]);
        Route::post('/delete/{id}', [
            'as' => 'users.delete',
            'uses' => 'UserController@delete',
            'middleware' => 'can:user-delete'
        ]);
        Route::get('/info', [
            'as' => 'users.info',
            'uses' => 'UserController@info',
        ]);
        Route::get('/editInfo/{id}', [
            'as' => 'users.editInfo',
            'uses' => 'UserController@editInfo',
        ]);
        Route::post('/updateInfo/{id}', [
            'as' => 'users.updateInfo',
            'uses' => 'UserController@updateInfo',
        ]);
    });

    Route::resource('leave_requests', 'LeaveRequestController')->only([
        'index', 'create', 'store',
    ]);
    Route::get('change-password', 'Auth\ChangePasswordController@index');
    Route::post('change-password', 'Auth\ChangePasswordController@store')->name('change.password');
    Route::post('leave_requests/deny/{id}', 'LeaveRequestController@deny')->name('leave_requests.deny');
    Route::get('leave_requests/accept/{id}', 'LeaveRequestController@accept')->name('leave_requests.accept');
    Route::get('change-language/{language}', 'HomeController@changeLanguage')->name('change-language');
});
